#!/usr/bin/env python

confs = {'default':{}}

confs['default'] = {
    'elecORDR':0.1,
    'muonORDR':0.1,
    'photonORDR':0.1,
    'minPtJet':20,
    'minPtElec':20,
    'minPtMuon':20,
    'minPtPhoton':20,
    'maxEtaJet':2.8,
    'maxEtaElec':2.5,
    'maxEtaMuon':2.7,
    'maxEtaPhoton':2.5,
    'dRBTag':0.4,
    'btag_frac':0.2,
    'dRIsolation':0.4,
    'jetMatchingDR':0.4,
    'matchingDR':{11:0.2, 13:0.2, 22:0.2},
    }
