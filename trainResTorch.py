#!/usr/bin/env python
"""@package docstring
This script trains an NN given an h5 file with object kinematics. 
"""

import sys, os, argparse, h5py, matplotlib, math, torch, torch.utils.data
import numpy as np
matplotlib.use('Agg')
import matplotlib.pyplot as plt

# Import all models and turn it into a dict. Not super elegant. All models start with the prefix 'model_'. We grab 
from plottingTools import *
from utils import *
import torchModels
modelDict = {}
for comp in dir(torchModels):
    if 'model_' in comp:
        modelDict[comp] = eval('torchModels.'+comp)
      
def train(data, modelName, modelFPath, hyperParams, sourceDSName, targetDSName, epochs, batchSize, hyperParamKey, sourceType):
    """! 
    This function trains a model given an h5 file with a training source and target data set. 
    The source is the data set that you want to make look more like the target, e.g. truth vs reco.
    """
    (xTrain, xTest, uniformityWeightsTrain, uniformityWeightsTest, recoJetsTrain, recoJetsTest, yTrainAll, yTestAll, inputScaler, resScalers, unmatchedValue) = data

    xTrain      = torch.autograd.Variable(torch.from_numpy(xTrain),requires_grad=False)
    xTest       = torch.autograd.Variable(torch.from_numpy(xTest),requires_grad=False)
    yTrainAll   = torch.autograd.Variable(torch.from_numpy(yTrainAll).to(dtype=torch.int64),requires_grad=False)
    yTestAll    = torch.autograd.Variable(torch.from_numpy(yTestAll).to(dtype=torch.int64),requires_grad=False)
                                    
    loss_fn = torch.nn.CrossEntropyLoss()
    for featI in range(yTrainAll.shape[1]):
        
        featName = featNames[featI]
        print("Starting to train model for", featName)
        weightsSavePath = modelFPath+sourceDSName+'_'+targetDSName+'_'+modelName+'_'+hyperParamKey+'_'+sourceType+"_"+featName+'_torch.h5'
        model = modelDict[modelName](xTrain.shape[1], hyperParams['outputDim'],
                                     outputAct=hyperParams['outputAct'],
                                     hiddenSpaceSizes=hyperParams['hiddenSpaceSize'],
                                     activations=hyperParams['activations']
        )
        optimizer = torch.optim.Adam(model.parameters(), lr=0.001)
        yTrain    = yTrainAll[:,featI,:]
        yTest     = yTestAll[:,featI,:]
        print(model)
        trainingSet = torch.utils.data.TensorDataset(xTrain, yTrain)
        testingSet  = torch.utils.data.TensorDataset(xTest, yTest)
        trainLoader = torch.utils.data.DataLoader(trainingSet, batch_size=batchSize, shuffle=True)
        testLoader  = torch.utils.data.DataLoader(testingSet, batch_size=batchSize, shuffle=True)
        trainIter = iter(trainLoader)
        testIter = iter(testLoader)

        trainLoss = []
        testLoss  =[]
        for epoch in range(epochs):  # loop over the dataset multiple running
            xTrainBatch, yTrainBatch = trainIter.next()
            xTestBatch, yTestBatch = testIter.next()
            yPredTrainBatch = model(xTrainBatch)
            loss = loss_fn(yPredTrainBatch, torch.argmax(yTrainBatch,1))
            trainLoss.append(loss.item())

            yPredTestBatch = model(xTestBatch)
            valLoss = loss_fn(yPredTestBatch, torch.argmax(yTestBatch,1))
            print(epoch, loss.item(), valLoss.item())
            testLoss.append(valLoss.item())
            
            # Zero the gradients before running the backward pass.
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
        
        print('Finished training model for feature', featName)

        torch.save(model.state_dict(), weightsSavePath)

        plt.clf()
        fig, ax = plt.subplots()
        plt.plot(trainLoss)
        plt.plot(testLoss)
        ax.set_xlabel('epoch')
        ax.set_ylabel('Loss')
        leg = plt.legend(['train', 'test'], loc=0, borderaxespad=0.)
        plt.savefig('loss_'+modelName+'_'+sourceType+"_"+featName+'.pdf', bbox_inches='tight')

def test(data, modelName, modelFPath, sourceDSName, targetDSName, hyperParamKey, sourceType, nResCats, printSum=False, nonResIndexStart=4, phiIndex=2):
    """! 
    This function tests a model given an h5 file with a training source and target data set. 
    """
    print("Loading data")
    (xTrain, xTest, uniformityWeightsTrain, uniformityWeightsTest, recoObjectsTrain, recoObjectsTest, yTrain, yTest, inputScaler, resScaler, unmatchedValue) = data
    
    xTrain      = torch.autograd.Variable(torch.from_numpy(xTrain),requires_grad=False)
    xTest       = torch.autograd.Variable(torch.from_numpy(xTest),requires_grad=False)
    yTrain   = torch.autograd.Variable(torch.from_numpy(yTrain).to(dtype=torch.int64),requires_grad=False)
    yTest    = torch.autograd.Variable(torch.from_numpy(yTest).to(dtype=torch.int64),requires_grad=False)
                                    
    print("Done loading data")
    binning = [i*0.01 for i in range(102)]
    profileBinning = [i*0.05 for i in range(25)]
    featBinning = [-0.02+i*0.02 for i in range(52)]

    corrTruthObjectsTest  = np.copy(xTest)
    corrTruthObjectsTrain = np.copy(xTrain)

    corrTruthObjectsTestOrigScale  = np.copy(xTest[:,:nonResIndexStart])
    corrTruthObjectsTrainOrigScale = np.copy(xTrain[:,:nonResIndexStart])

    recoObjectsTestOrigScale  = np.copy(xTest[:,:nonResIndexStart])
    recoObjectsTrainOrigScale = np.copy(xTrain[:,:nonResIndexStart])

    # This uses some sensible binning of the original scales
    # and put the inefficiency in an underflow bin.
    yGenTrain = np.zeros(yTrain.shape)
    yGenTest = np.zeros(yTest.shape)

    unmatchedValuesOrigScale = [0 for i in range(yTrain.shape[1])]
    models = []
        
    # We have a model for each resolution we want to model. Loop over resolutions and make predictions that will be
    # store in numpy arrays.
    
    for featI in range(yTrain.shape[1]):
        featName = featNames[featI]
        print("Loading model for", featName)
        modelWeightsPath = modelFPath+sourceDSName+'_'+targetDSName+'_'+modelName+'_'+hyperParamKey+'_'+sourceType+"_"+featName+'_torch.h5'
        
        model = modelDict[modelName](xTrain.shape[1], hyperParams['outputDim'],
                                     outputAct=hyperParams['outputAct'],
                                     hiddenSpaceSizes=hyperParams['hiddenSpaceSize'],
                                     activations=hyperParams['activations']
        )
        # Load weights into new model
        model.load_state_dict(torch.load(modelWeightsPath))
        model.eval()
        print("Done loading from", modelWeightsPath)
        if printSum:
            model.summary()
        models.append(model)
        
        unmatchedValuesOrigScale[featI] = origScaleBinning[featI][1]

    # Let's first get do the prediction on the first feature which always has an inefficiency category, i.e. a bin in the binned pdf
    # for which an object failed to be reconstructed. If the random sampling of the first feature falls into this bin we don't have to do
    # the other feature samplings.
    unmatchedIndicesTest, yGenTest[:,0] = transformObjects(xTest, models[0], corrTruthObjectsTestOrigScale, inputScaler, unmatchedValuesOrigScale[0], 0, nResCats, resScaler, 0, modelType='torch')
    matchedXTest = xTest[~unmatchedIndicesTest]
    for featI in range(1,yTrain.shape[1]):
        dummy, yGenTest[~unmatchedIndicesTest,featI] = transformObjects(matchedXTest, models[featI], corrTruthObjectsTestOrigScale, inputScaler, unmatchedValuesOrigScale[featI], 1, nResCats, resScaler, featI, unmatchedIndicesTest, modelType='torch')
        
    unmatchedIndicesTrain, yGenTrain[:,0] = transformObjects(xTrain, models[0], corrTruthObjectsTrainOrigScale, inputScaler, unmatchedValuesOrigScale[0], 0, nResCats, resScaler, 0)
    matchedXTrain = xTrain[~unmatchedIndicesTrain]
    for featI in range(1,yTrain.shape[1]):
        dummy, yGenTrain[~unmatchedIndicesTrain,featI] = transformObjects(matchedXTrain, models[featI], corrTruthObjectsTrainOrigScale, inputScaler, unmatchedValuesOrigScale[featI], 1, nResCats, resScaler, featI, unmatchedIndicesTrain, modelType='torch')

    # Now transform the other features only for 
    # Transform the inputs into the original scale now so we can use them to get generated NN based reco objects.    
    truthObjectsTestOrigScale  = inputScaler.inverse_transform(xTest)
    truthObjectsTrainOrigScale = inputScaler.inverse_transform(xTrain)

    # We should only transform matched objects
    unmatchedIndicesRecoTest = recoObjectsTest[:,0]<0;
    unmatchedIndicesRecoTrain = recoObjectsTrain[:,0]<0;

    recoObjectsTestOrigScale[~unmatchedIndicesRecoTest]  = inputScaler.inverse_transform(recoObjectsTest[~unmatchedIndicesRecoTest])[:,:nonResIndexStart]
    recoObjectsTrainOrigScale[~unmatchedIndicesRecoTrain] = inputScaler.inverse_transform(recoObjectsTrain[~unmatchedIndicesRecoTrain])[:,:nonResIndexStart]

    for featI in range(yTrain.shape[1]):
        recoObjectsTestOrigScale[unmatchedIndicesRecoTest]  = unmatchedValuesOrigScale[featI]
        recoObjectsTrainOrigScale[unmatchedIndicesRecoTrain] = unmatchedValuesOrigScale[featI]
    
    lowEtaIdx = abs(truthObjectsTestOrigScale[:,1]) < 0.4
    highEtaIdx = abs(truthObjectsTestOrigScale[:,1]) > 0.8
       
    realRes = np.argmax(yTest, axis=2)*1.0/yTest.shape[2]
    genRes  = np.argmax(yGenTest, axis=2)*1.0/yGenTest.shape[2]

    realResOrigShape = resScaler.inverse_transform(realRes)
    genResOrigShape  = resScaler.inverse_transform(genRes)

    lowEtaRealResHist = np.sum(yTest[lowEtaIdx], axis=0)
    highEtaRealResHist = np.sum(yTest[highEtaIdx], axis=0)

    lowEtaGenResHist = np.sum(yGenTest[lowEtaIdx], axis=0)
    highEtaGenResHist = np.sum(yGenTest[highEtaIdx], axis=0)

    realResHist = np.sum(yTest, axis=0)
    genResHist  = np.sum(yGenTest, axis=0)

    rebinFact = 5
    lowEtaGenResHistRebin = lowEtaGenResHist[0,1:].reshape(-1,rebinFact).sum(axis=1)[1:]
    highEtaGenResHistRebin = highEtaGenResHist[0,1:].reshape(-1,rebinFact).sum(axis=1)[1:]
    lowEtaRealResHistRebin = lowEtaRealResHist[0,1:].reshape(-1,rebinFact).sum(axis=1)[1:]
    highEtaRealResHistRebin = highEtaRealResHist[0,1:].reshape(-1,rebinFact).sum(axis=1)[1:]

    # This really ugly.
    rebinning = [i*0.05 for i in range(20)]
    compareDists([lowEtaRealResHistRebin, lowEtaGenResHistRebin, highEtaRealResHistRebin, highEtaGenResHistRebin],
                 ['Delphes, $|\eta|<0.4$', 'NN, $|\eta|<0.4$', 'Delphes, $|\eta|>0.8$', 'NN, $|\eta|>0.8$'],
                 np.array(rebinning), ['k', 'k', 'r', 'r'], 'jet', 'pTResLowEta', ratioLabels[0]+' [scaled]',
                 ratioYLabel="Delphes/NN", markerstyles=['o', '+', 'o', '+'],
                 markerfacecolors=['none', 'none', 'none', 'none'],
                 plotRatio=False, normalize=True, yLogLowRange=0.01)
    
    compareDists([lowEtaRealResHistRebin, lowEtaGenResHistRebin, highEtaRealResHistRebin, highEtaGenResHistRebin],
                 ['Delphes, $|\eta|<0.4$', 'NN, $|\eta|<0.4$', 'Delphes, $|\eta|>0.8$', 'NN, $|\eta|>0.8$'],
                 np.array(rebinning), ['k', 'k', 'r', 'r'], 'jet', 'pTResLowEta', ratioLabels[0]+' [scaled]',
                 ratioYLabel="Delphes/NN", markerstyles=['o', '+', 'o', '+'],
                 markerfacecolors=['none', 'none', 'none', 'none'],
                 plotRatio=False, log=False,
                 plotHist=[False, True, False, True], plotErr=[True, False, True, False], normalize=True)
   
    # Now let's make some plots.
    for featI in range(yTrain.shape[1]):
        featName       = featNames[featI]
        prettyVarLabel    = ratioLabels[featI]
        rmsLabel          = rmsLabels[featI]
        featLabel      = featLabels[featI]
        featLabelTruth = featLabelsTruth[featI]
        
        compareDists([realResHist[featI,:], genResHist[featI,:]], ['Delphes', 'NN'],
                     np.array(binning), ['k', 'r'], 'jet',
                     featName+'Res', prettyVarLabel, log=False, ratioYLabel="Delphes/NN",
                     normalize=True, plotRatio=False)

        # Let's plot the NN output for some random events.
        randObj = np.random.randint(xTest.shape[0], size=3)
        plt.clf()
        fig, ax = plt.subplots()
        for randI in range(len(randObj)):
            ax.plot([i for i in range(nResCats)], yGenTest[randObj[randI],featI,:], label='Rand jet '+str(randI), linewidth=5)
        ax.set_xlabel('NN category for '+prettyVarLabel)
        ax.set_ylabel('Category probability')
        ax.legend(loc=0, borderaxespad=0.)
        plt.savefig('nnOutput_'+featName+'.pdf', bbox_inches='tight')
        ax.set_yscale('log')
        yRange = (0.0001, ax.get_ylim()[1])
        ax.set_ylim(yRange)
        plt.savefig('nnOutput_'+featName+'_log.pdf', bbox_inches='tight')

        # Now in the original.
        recoObjectsTestHist, featBinEdges      = np.histogram(recoObjectsTestOrigScale[:,featI], origScaleBinning[featI])
        truthObjectsTestHist, featBinEdges     = np.histogram(truthObjectsTestOrigScale[:,featI], origScaleBinning[featI])
        corrTruthObjectsTestHist, featBinEdges = np.histogram(corrTruthObjectsTestOrigScale[:,featI], origScaleBinning[featI])

        recoObjectsTrainHist, featBinEdges      = np.histogram(recoObjectsTrainOrigScale[:,featI], origScaleBinning[featI])
        truthObjectsTrainHist, featBinEdges     = np.histogram(truthObjectsTrainOrigScale[:,featI], origScaleBinning[featI])
        corrTruthObjectsTrainHist, featBinEdges = np.histogram(corrTruthObjectsTrainOrigScale[:,featI], origScaleBinning[featI])

        compareDists([recoObjectsTestHist, corrTruthObjectsTestHist, truthObjectsTestHist], ['Delphes', 'NN', 'Generator'],
                     featBinEdges, ['k', 'b', 'r'], 'jet', featName+'_genVsReco_origScale_test',
                     featLabel, rounding=varRounding[featI], units=units[featI], ratioEndIndex=1,
                     ratioYLabel="Delphes/NN")
        compareDists([recoObjectsTestHist, corrTruthObjectsTestHist, truthObjectsTestHist], ['Delphes', 'NN', 'Generator'],
                     featBinEdges, ['k', 'b', 'r'], 'jet', featName+'_genVsReco_origScale_test',
                     featLabel, log=False, rounding=varRounding[featI], units=units[featI],
                     ratioEndIndex=1, ratioYLabel="Delphes/NN")

        compareDists([recoObjectsTrainHist, corrTruthObjectsTrainHist, truthObjectsTrainHist], ['Delphes', 'NN', 'Generator'],
                     featBinEdges, ['k', 'b', 'r'], 'jet', featName+'_genVsReco_origScale_train',
                     featLabel, rounding=varRounding[featI], units=units[featI], ratioEndIndex=1,
                     ratioYLabel="Delphes/NN")
        compareDists([recoObjectsTrainHist, corrTruthObjectsTrainHist, truthObjectsTrainHist], ['Delphes', 'NN', 'Generator'],
                     featBinEdges, ['k', 'b', 'r'], 'jet', featName+'_genVsReco_origScale_train',
                     featLabel, log=False, rounding=varRounding[featI], units=units[featI],
                     ratioEndIndex=1, ratioYLabel="Delphes/NN")


if __name__ == '__main__':    
    parser = argparse.ArgumentParser(description='Train a NN for regression.')
    parser.add_argument('trainTest', help="Perform training, testing, or both.")
    parser.add_argument('--h5Path', help='Location of training data files.', default='rfast008.h5')
    parser.add_argument('--sourceType', help='What is the source of the data: Delphes or toy detector.', default='Delphes', choices=['Delphes', 'toy', 'other'])
    parser.add_argument('--nFiles', type=int, help='How many files do we want to process', default=None)
    parser.add_argument('--nObjects', type=int, help='Number of objects', default=2000000)
    parser.add_argument('--modelName', help='Name of model.', default='model_multiClass')
    parser.add_argument('--sourceDSName', help='Name of source data set (the data set you want to transform).', default='truthJets')
    parser.add_argument('--targetDSName', help='Name of target data set (the data set you want another dataset to mimic).', default='recoJets')
    parser.add_argument('--modelFPath', default='models/', help='Path where model should be saved.')
    parser.add_argument('--nTrainSamp', type=int, help='Number of objects to use for training', default=200000)
    parser.add_argument('--nTestSamp', type=int, help='Number of objects to use for testing', default=100000)
    parser.add_argument('--epochs', type=int, help='Number of epochs', default=20)
    parser.add_argument('--batchSize', type=int, help='Batch size', default=100)
    parser.add_argument('--nonResIndexStart', type=int, help='What is the index where non resolution variables start', default=4)
    parser.add_argument('--randomSeed', type=int, help='Seed for randomizor', default=18)
    parser.add_argument('--nResCats', type=int, help='Number of resolution categories', default=101)
    parser.add_argument('--uniformityProp', type=int, help='Property which will be flattened during training. For example, we may want a flat pT distribution.', default=None)
    parser.add_argument('--hyperParamFPath', type=str, help='Path to pickle file containing the hyper parameters', default='hyperParamsTest.pkl')
    parser.add_argument('--hyperParamKey', type=str, help='Suffix added to file names. Useful for hyperparameter scan.', default='resMultiC')
    parser.add_argument('--updateHyperparams', action='store_true', help='Update the hyperparameter pickle file.')
   
    args = parser.parse_args()

    # Let's load the hyperparameter set we need.
    if args.updateHyperparams:
        print("Producing new hyperparameter file.")
        produceHyperParams(args.hyperParamFPath)
        allHyperParams = pickle.load(open(args.hyperParamFPath, 'rb'))
    else:
        try:
            allHyperParams = pickle.load(open(args.hyperParamFPath, 'rb'))
        except IOError as e:
            print("Couldn't load "+args.hyperParamFPath)
            print("Producing new hyperparameter file.")
            produceHyperParams(args.hyperParamFPath)
            allHyperParams = pickle.load(open(args.hyperParamFPath, 'rb'))
    
    if args.hyperParamKey not in allHyperParams:
        print("Couldn't find hyperparameter set with key", args.hyperParamKey)
        sys.exit()
        
    hyperParams = allHyperParams[args.hyperParamKey]

    np.random.seed(args.randomSeed)
    # Load training and testing data
    data = prepMatchedDataRes(args.h5Path, args.nTrainSamp, args.nTestSamp, args.randomSeed,
                              sourceType=args.sourceType, nFiles=args.nFiles,
                              nObjects=args.nObjects,uniformityProp=args.uniformityProp,
                              nonResIndexStart=args.nonResIndexStart);
   
    if 'train' in args.trainTest:
        train(data, args.modelName, args.modelFPath, hyperParams, args.sourceDSName, args.targetDSName,
              args.epochs, args.batchSize, args.hyperParamKey, args.sourceType
        );
    if 'test' in args.trainTest:
        test(data, args.modelName, args.modelFPath, args.sourceDSName, args.targetDSName,
             args.hyperParamKey, args.sourceType, args.nResCats
        );
    

