#!/usr/bin/env python

import argparse, h5py, ROOT, math
from ROOT import TChain
import numpy as np
import convConfs
from glob import glob
from utils import mpiToPi
ROOT.gSystem.Load("Delphes-3.4.1/libDelphes")

try:
  ROOT.gInterpreter.Declare('#include "Delphes-3.4.1/classes/DelphesClasses.h"')
  ROOT.gInterpreter.Declare('#include "Delphes-3.4.1/external/ExRootAnalysis/ExRootTreeReader.h"')
except:
  pass

  
def fillArray(entry, branch, array, objTrunc):
    """! 
    Fill a numpy array from a ExRootAnalysis array.
    """
    nObjects = branch.GetEntries()
    if nObjects > objTrunc:
        nObjects = objTrunc
    for objectI in range(nObjects):
        physObj = branch.At(objectI)
        array[entry,objectI] = (physObj.PT, physObj.Eta, physObj.Phi, physObj.Mass)

def overlapRemoval(refObj, branches, dRs):
  """!
  Check of refObj overlaps with any of the objects in objects.
  """
  overlaps = [False for i in branches]
  for objType in range(len(branches)):
    objBranch = branches[objType]
    for objI in range(objBranch.GetEntries()):
      physObj = objBranch.At(objI)
      dR = calcDR(physObj,refObj)
      if dR < dRs[objType]:
        overlaps[objType] = True;
        break;
  return overlaps

def calcDR(obj1, obj2):
  """!
  Calculated the delta R of two Delphes objects.
  """
  dEta = obj1.Eta-obj2.Eta;
  dPhi = mpiToPi(obj1.Phi-obj2.Phi)
  return math.sqrt(dPhi**2+dEta**2)
  
def convert(inFPaths, nFilesPerPath, treeName, h5FName, confKey):
    """! 
    Convert a series of ROOT files with funky Delphes trees to hdf5.
    """
    conf = convConfs.confs[confKey]
    
    chain = TChain(treeName)
    for inFPath, nFiles in zip(inFPaths,nFilesPerPath):
      inFNames = glob(inFPath+"*.root")
      for inFName in inFNames[:nFiles]:
        print "Adding", inFName, "to the paths."
        chain.Add(inFName);
    # Create object of class ExRootTreeReader
    treeReader = ROOT.ExRootTreeReader(chain)

    # Get pointers to branches used in this analysis
    branchEvent      = treeReader.UseBranch("Event")
    branchGenMET     = treeReader.UseBranch("GenMissingET")  
    branchMET        = treeReader.UseBranch("MissingET")  
    branchHT         = treeReader.UseBranch("ScalarHT")  
    branchRecoJet    = treeReader.UseBranch("Jet")
    branchTruthJet   = treeReader.UseBranch("GenJet")
    branchRecoElec   = treeReader.UseBranch("Electron")
    branchRecoMuon   = treeReader.UseBranch("Muon")
    branchRecoPhoton = treeReader.UseBranch("Photon")
    branchParticle   = treeReader.UseBranch("Particle")

    nEvents = treeReader.GetEntries()
    dtype = [('type', '<i4'), ('pt', '<f4'), ('eta', '<f4'), ('phi', '<f4'), ('mass', '<f4'), ('iso', '<f4'), ('btag','<f4'), ('evtID', '<i4'), ('GenMET', '<f4'), ('MET', '<f4'), ('HT', '<f4')]

    # Initiate the array to be large before we fill them because numpy is slow about appending.
    recoObjArray = np.recarray((0,len(dtype)), dtype=dtype)
    truthObjArray = np.recarray((0,len(dtype)), dtype=dtype)
    
    nOneMatch = 0;
    nMultiMatch = 0;
    for entry in xrange(nEvents):
      treeReader.ReadEntry(entry)
      if entry % 1000 == 0:
        print entry
      # Let's get the truth jets and get a measure on whether they are likely to be btagged by the fraction of pT came from a b-quark.
      goodTruthJets = []
      
      for truthJetI in xrange(branchTruthJet.GetEntries()):
        truthJet = branchTruthJet.At(truthJetI)
        if truthJet.PT < conf['minPtJet'] or abs(truthJet.Eta)>conf['maxEtaJet'] or truthJet.Mass<1:
          continue;
        # get fraction of b-quark inside this jet (in %)
	btag_fracmom=0;
	for partI in xrange(branchParticle.GetEntriesFast()):
	  ph = branchParticle.At(partI);
	  pdgCode = abs(ph.PID);
	  if (pdgCode != 5):
            continue;
	  dR = calcDR(truthJet, ph)
	  rat  = (ph.PT / truthJet.PT);
	  if dR<conf['dRBTag'] and rat>conf['btag_frac']:
            btag_fracmom= 1000 * rat;
  
        goodTruthJets.append((truthJet, btag_fracmom))
        truthObjArray.resize(truthObjArray.shape[0]+1)
        truthObjArray[truthObjArray.shape[0]-1] = (0, truthJet.PT, truthJet.Eta, truthJet.Phi, truthJet.Mass, -99, int(btag_fracmom), entry, branchGenMET.At(0).MET, branchMET.At(0).MET, branchHT.At(0).HT)
        
      truthObjects = {}
      #Get truth leptons and photons
      for partI in xrange(branchParticle.GetEntriesFast()):
        ph = branchParticle.At(partI);
        pdgCode = abs(ph.PID);
        status = ph.Status;
        if (status  != 1):
          continue;
        if (pdgCode != 13 and pdgCode != 11 and pdgCode != 22):
          continue; 

        #get fraction of energy in the cone 0.3 around the candidate 
        ptsum=0;
        for partI2 in xrange(branchParticle.GetEntriesFast()):
          gen = branchParticle.At(partI2);
          stat = gen.Status;
          if (stat != 1):
            continue;

          dR   = calcDR(gen,ph)
          if (dR<conf['dRIsolation']):
            ptsum+=gen.PT;  
        if pdgCode in truthObjects:
          truthObjects[pdgCode].append((ph,ptsum))
        else:
          truthObjects[pdgCode] = [(ph,ptsum)]
        
        truthObjArray.resize(truthObjArray.shape[0]+1)
        truthObjArray[truthObjArray.shape[0]-1] = (pdgCode, ph.PT, ph.Eta, ph.Phi, ph.Mass, ptsum, -99, entry, branchGenMET.At(0).MET, branchMET.At(0).MET, branchHT.At(0).HT)

      goodRecoJets = []
      # Do some reco overlap removal for jets only (assume muons, photons, and electrons don't overlap).
      for recoJetI in xrange(branchRecoJet.GetEntries()):
        recoJet = branchRecoJet.At(recoJetI)
        if recoJet.PT < conf['minPtJet'] or abs(recoJet.Eta)>conf['maxEtaJet'] or recoJet.Mass<1:
          continue;

        overlaps = overlapRemoval(recoJet, [branchRecoElec, branchRecoMuon, branchRecoPhoton], [conf['elecORDR'], conf['muonORDR'], conf['photonORDR']])
        if not any(overlaps):
          goodRecoJets.append(recoJet)

      # Get the reco muons, electrons, and photons. No overlap done (maybe we should do this)
      recoObjects = {};
      pdgCode = 11
      for recoElecI in xrange(branchRecoElec.GetEntries()):
        recoElec = branchRecoElec.At(recoElecI)
        if recoElec.PT < conf['minPtElec'] or abs(recoElec.Eta)>conf['maxEtaElec']:
          continue;
        if pdgCode in recoObjects:
          recoObjects[pdgCode].append(recoElec)
        else:
          recoObjects[pdgCode] = [recoElec]

      pdgCode = 13
      for recoMuonI in xrange(branchRecoMuon.GetEntries()):
        recoMuon = branchRecoMuon.At(recoMuonI)
        if recoMuon.PT < conf['minPtMuon'] or abs(recoMuon.Eta)>conf['maxEtaMuon']:
          continue;
        if pdgCode in recoObjects:
          recoObjects[pdgCode].append(recoMuon)
        else:
          recoObjects[pdgCode] = [recoMuon]

      pdgCode = 22
      for recoPhotonI in xrange(branchRecoPhoton.GetEntries()):
        recoPhoton = branchRecoPhoton.At(recoPhotonI)
        if recoPhoton.PT < conf['minPtPhoton'] or abs(recoPhoton.Eta)>conf['maxEtaPhoton']:
          continue;
        if pdgCode in recoObjects:
          recoObjects[pdgCode].append(recoPhoton)
        else:
          recoObjects[pdgCode] = [recoPhoton]
       
      # Now we have to do truth matching and make a junk object if there is no matching.      
      for truthJet, btag_fracmom in goodTruthJets:
        matches = []
        for recoJet in goodRecoJets:
          dR = calcDR(truthJet, recoJet)
          if dR < conf['jetMatchingDR']:
            matches.append((recoJet,dR))
        # if we find more than one object pick the reco object with the smallest DR.
        # This isn't the best idea (one truth object essentially turns into two reco objects)
        # since we aren't taking the efficiency of this splitting into account.
        recoObjArray.resize(recoObjArray.shape[0]+1)
        if len(matches) > 0:
          minDR = 9999
          for match, dR in matches:
            if dR < minDR:
              minDR = dR
              minDRMatch = match
          bestMatch = minDRMatch
          # 
          recoObjArray[recoObjArray.shape[0]-1] = (0, bestMatch.PT, bestMatch.Eta, bestMatch.Phi, bestMatch.Mass, -99, bestMatch.BTag, entry, branchGenMET.At(0).MET, branchMET.At(0).MET, branchHT.At(0).HT)
        # If there is no match fill a junk jet with unique values (-99) for the various quantities
        else:
          recoObjArray[recoObjArray.shape[0]-1] = (0, -99, -99, -99, -99, -99, -99, entry, branchGenMET.At(0).MET, branchMET.At(0).MET, branchHT.At(0).HT)

      # Now the other objects (maybe these shouldn't be treated differently from jets?
      for pdgCode in truthObjects:
        for truthObject, ptsum in truthObjects[pdgCode]:
          matches = []
          if pdgCode in recoObjects:
            for recoObject in recoObjects[pdgCode]:
              dR = calcDR(truthObject, recoObject)
              if dR < conf['matchingDR'][pdgCode]:
                matches.append((recoObject,dR))
          # if we find more than one object pick the reco object with the smallest DR.
          # This isn't the best idea (one truth object essentially turns into two reco objects)
          # since we aren't taking the efficiency of this splitting into account.
          recoObjArray.resize(recoObjArray.shape[0]+1)
          if len(matches) > 0:
            minDR = 9999
            for match, dR in matches:
              if dR < minDR:
                minDR = dR
                minDRMatch = match
            bestMatch = minDRMatch    # Assume some max number of objects and multiply this by the number of events

            recoObjArray[recoObjArray.shape[0]-1] = (pdgCode, bestMatch.PT, bestMatch.Eta, bestMatch.Phi, 0, -99, -99, entry, branchGenMET.At(0).MET, branchMET.At(0).MET, branchHT.At(0).HT)
          # If there is no match fill a junk jet with unique values (-99) for the various quantities
          else:
            recoObjArray[recoObjArray.shape[0]-1] = (pdgCode, -99, -99, -99, -99, -99, -99, entry, branchGenMET.At(0).MET, branchMET.At(0).MET, branchHT.At(0).HT)

    h5f = h5py.File(h5FName, 'w');
    h5f.create_dataset('recoObjects', data=recoObjArray)
    h5f.create_dataset('truthObjects', data=truthObjArray)
    h5f.close();
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Convert Delphes information to HDF5 file.')
    parser.add_argument('--inFPaths', help='Location of ROOT files, comma delimited', default='/netapp/data/hepsim/events/pp/14tev/mg5_ttbar_jet/rfast008/*,/netapp/data/hepsim/events/pp/14tev/pythia8_gammajets_wgt/rfast008/*')
    parser.add_argument('--nFilesPerPath', help='Number of files to read in per inFPath, comma delimited', default="10,10")
    parser.add_argument('--treeName', help='Name of tree to be converted.', default='Delphes')
    parser.add_argument('--h5FName', help='Name of output h5 file.', default='rfast008_new.h5')
    parser.add_argument('--confKey', help='Configuration key that corresponds to the key for the dictionary that holds various cuts', default='default')

    args = parser.parse_args()
    inFPaths = args.inFPaths.split(',')
    nFilesPerPath = [int(nFiles) for nFiles in args.nFilesPerPath.split(',')]
    convert(inFPaths, nFilesPerPath, args.treeName, args.h5FName, args.confKey)
