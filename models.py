#!/usr/bin/env python
from keras.models import Sequential, Model
from keras.layers.core import Dense, Activation, Dropout, Flatten
from keras.layers.normalization import BatchNormalization
from keras.layers import Conv1D, Conv2D, MaxPooling2D, Input, Reshape
from keras.layers.merge import Concatenate, _Merge
from keras.layers.advanced_activations import LeakyReLU, PReLU
from keras.layers import LSTM, Flatten, GRU
from keras.layers import LeakyReLU
from keras import optimizers
from keras import initializers
from keras import backend as K

import numpy as np

class RandomWeightedAverage(_Merge):
    """
    Takes a randomly-weighted average of two tensors. In geometric terms, this outputs a random point on the line
    between each pair of input points.
    Inheriting from _Merge is a little messy but it was the quickest solution I could think of.
    Improvements appreciated.
    """    
    def _merge_function(self, inputs):
        weights = K.random_uniform((100, 1, 1, 1))
        return (weights * inputs[0]) + ((1 - weights) * inputs[1])


def wasserstein_loss(y_true, y_pred):
    """Calculates the Wasserstein loss for a sample batch.
    The Wasserstein loss function is very simple to calculate. In a standard GAN, the discriminator
    has a sigmoid output, representing the probability that samples are real or generated. In Wasserstein
    GANs, however, the output is linear with no activation function! Instead of being constrained to [0, 1],
    the discriminator wants to make the distance between its output for real and generated samples as large as possible.
    The most natural way to achieve this is to label generated samples -1 and real samples 1, instead of the
    0 and 1 used in normal GANs, so that multiplying the outputs by the labels will give you the loss immediately.
    Note that the nature of this loss means that it can be (and frequently will be) less than 0."""
    return K.mean(y_true * y_pred)

def gradient_penalty_loss(y_true, y_pred, averaged_samples, gradient_penalty_weight):
    """Calculates the gradient penalty loss for a batch of "averaged" samples.
    In Improved WGANs, the 1-Lipschitz constraint is enforced by adding a term to the loss function
    that penalizes the network if the gradient norm moves away from 1. However, it is impossible to evaluate
    this function at all points in the input space. The compromise used in the paper is to choose random points
    on the lines between real and generated samples, and check the gradients at these points. Note that it is the
    gradient w.r.t. the input averaged samples, not the weights of the discriminator, that we're penalizing!
    In order to evaluate the gradients, we must first run samples through the generator and evaluate the loss.
    Then we get the gradients of the discriminator w.r.t. the input averaged samples.
    The l2 norm and penalty can then be calculated for this gradient.
    Note that this loss function requires the original averaged samples as input, but Keras only supports passing
    y_true and y_pred to loss functions. To get around this, we make a partial() of the function with the
    averaged_samples argument, and use that for model training."""
    # first get the gradients:
    #   assuming: - that y_pred has dimensions (batch_size, 1)
    #             - averaged_samples has dimensions (batch_size, nbr_features)
    # gradients afterwards has dimension (batch_size, nbr_features), basically
    # a list of nbr_features-dimensional gradient vectors
    gradients = K.gradients(y_pred, averaged_samples)[0]
    # compute the euclidean norm by squaring ...
    gradients_sqr = K.square(gradients)
    #   ... summing over the rows ...
    gradients_sqr_sum = K.sum(gradients_sqr,
                              axis=np.arange(1, len(gradients_sqr.shape)))
    #   ... and sqrt
    gradient_l2_norm = K.sqrt(gradients_sqr_sum)
    # compute lambda * (1 - ||grad||)^2 still for each single sample
    gradient_penalty = gradient_penalty_weight * K.square(1 - gradient_l2_norm)
    # return the mean as loss over all the batch samples
    return K.mean(gradient_penalty)

def model_MLP_MSE(inputShape, outputDim, savePath, outputAct, hiddenSpaceSize=[50], activations=['relu']):
    model = Sequential()
    model.add(Dense(hiddenSpaceSize[0], input_shape=inputShape, activation=activations[0]))
    for layerI in range(1, len(hiddenSpaceSize)):
        model.add(Dense(hiddenSpaceSize[layerI], activation=activations[layerI]))

    model.add(Dense(outputDim, activation=outputAct))
    
    # Set loss and optimizer
    model.compile(loss='mean_squared_error', optimizer='adam')
    
    # Store model to file
    model_json = model.to_json()
    with open(savePath, "w") as json_file:
        json_file.write(model_json)
    model.summary()
    return model

def model_multiClass(inputShape, outputDim, savePath, outputAct, hiddenSpaceSize=[50], activations=['relu']):
    model = Sequential()
    model.add(Dense(hiddenSpaceSize[0], input_shape=inputShape, activation=activations[0]))
    for layerI in range(1, len(hiddenSpaceSize)):
        model.add(Dense(hiddenSpaceSize[layerI], activation=activations[layerI]))

    model.add(Dense(outputDim, activation=outputAct))
    
    # Set loss and optimizer
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['acc'])
    
    # Store model to file
    model_json = model.to_json()
    with open(savePath, "w") as json_file:
        json_file.write(model_json)
    model.summary()
    return model


def model_ae(inputShape, outputDim, savePath, outputAct, hiddenSpaceSize=[50], activations=['relu']):
    model = Sequential()
    model.add(Dense(hiddenSpaceSize[0], input_shape=inputShape, activation=activations[0]))
    for layerI in range(1, len(hiddenSpaceSize)):
        model.add(Dense(hiddenSpaceSize[layerI], activation=activations[layerI]))

    model.add(Dense(outputDim, activation=outputAct))

    # Set loss and optimizer
    #model.compile(loss='binary_crossentropy', optimizer='adam')
    model.compile(loss='mean_squared_error', optimizer='adam')

    model.summary()
    # Store model to file
    model_json = model.to_json()
    with open(savePath, "w") as json_file:
        json_file.write(model_json)
    
    return model


def model_gan_gen(inputDim, outputDim, savePath, outputAct='linear', hiddenSpaceSize=[50,50], activations=['relu', 'relu']):
    model = Sequential()
    model.add(Dense(hiddenSpaceSize[0], input_dim=inputDim, activation=activations[0], kernel_initializer='he_normal'))
    for layerI in range(1, len(hiddenSpaceSize)):
        model.add(Dense(hiddenSpaceSize[layerI], activation=activations[layerI], kernel_initializer='he_normal'))

    model.add(Dense(outputDim, activation=outputAct, kernel_initializer='he_normal'))

    truthObj = Input(shape=(inputDim,))
    genRecoObj = model(truthObj)

    returnModel = Model(truthObj, genRecoObj)
    
    # Store model to file
    model_json = returnModel.to_json()
    with open(savePath, "w") as json_file:
        json_file.write(model_json)
        
    return returnModel
    #return model

def model_gan_discr(inputShape, outputDim, savePath, outputAct='sigmoid', hiddenSpaceSize=[50,50], activations=['relu', 'relu']):
    model = Sequential()
    model.add(Dense(hiddenSpaceSize[0], input_shape=inputShape, activation=activations[0], kernel_initializer='he_normal'))
    for layerI in range(1, len(hiddenSpaceSize)):
        model.add(Dense(hiddenSpaceSize[layerI], activation=activations[layerI], kernel_initializer='he_normal'))

    model.add(Dense(outputDim, activation=outputAct, kernel_initializer='he_normal'))

    
    obj = Input(shape=inputShape)
    validity = model(obj)

    returnModel = Model(obj, validity)
    # Store model to file
    model_json = returnModel.to_json()
    with open(savePath, "w") as json_file:
        json_file.write(model_json)

    return returnModel

    
