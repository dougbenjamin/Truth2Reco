#!/usr/bin/env python
"""@package docstring
This script trains an NN given an h5 file with object kinematics. 
"""

import sys, os, argparse, h5py, matplotlib, math
import numpy as np
from copy import deepcopy
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from keras.models import model_from_json
from keras.layers import Input
from keras.models import Sequential, Model
import cPickle as pickle
from plottingTools import *
from utils import *

# Import all models and turn it into a dict. Not super elegant. All models start with the prefix 'model_'. We grab 
import models
modelDict = {}
for comp in dir(models):
    if 'model_' in comp:
        modelDict[comp] = eval('models.'+comp)


# Import all training functions and make them into a dict. I know, eval is evil but I need something quick.
import trainGAN
trainingDict = {}
for trFunc in dir(trainGAN):
    if 'train' in trFunc:
        trainingDict[trFunc] = eval('trainGAN.'+trFunc)

def test(data, modelName, modelFPath, sourceDSName, targetDSName, hyperParamKey, sourceType):
    """! 
    This function plots the generator and critic loss of a model given.
    It also plots the true distributions and compares them to the generated ones.
    """
    (xTrain, xTest, uniformityWeightsTrain, uniformityWeightsTest, yTrain, yTest, scaler) = data
    jSONPathGen = modelFPath+sourceDSName+'_'+targetDSName+'_'+modelName+'_'+hyperParamKey+'_'+sourceType+'_gen.json'
    weightsPathGen = modelFPath+sourceDSName+'_'+targetDSName+'_'+modelName+'_'+hyperParamKey+'_'+sourceType+'_gen.h5'
    json_file = open(jSONPathGen, 'r')
    model_json = json_file.read()
    json_file.close()
    generator = model_from_json(model_json)
    generator.summary()
    generator.load_weights(weightsPathGen)
    generator.summary()

    yGenTest = generator.predict(xTest);
    yGenTrain = generator.predict(xTrain);

    # Remember that our inputs are randomNumber, truthJetFeature1, truthJetFeature2, etc. We don't want to rescale the random number.
    truthJetsScaledTest = xTest[:,:-1]
    truthJetsScaledTrain = xTrain[:,:-1]
    truthJetsTest = scaler.inverse_transform(truthJetsScaledTest)
    truthJetsTrain = scaler.inverse_transform(truthJetsScaledTrain)

    # Scale reco jets
    recoJetsScaledTest = yTest
    recoJetsScaledTrain = yTrain
    recoJetsTest = scaler.inverse_transform(recoJetsScaledTest)
    recoJetsTrain = scaler.inverse_transform(recoJetsScaledTrain)

    # Scale generated reco jets.
    corrTruthScaledTest = yGenTest
    corrTruthScaledTrain = yGenTrain
    corrTruthTest = scaler.inverse_transform(corrTruthScaledTest)
    corrTruthTrain = scaler.inverse_transform(corrTruthScaledTrain)

    nSteps = 50
    minScaledVal = 0.1
    maxScaledVal = 1.1
    # Step size for scaled parameters. 
    stepSize = (maxScaledVal-minScaledVal)/nSteps

    featBinning = [stepSize*(i-3)+minScaledVal for i in range(nSteps+5)]
    for featI in range(recoJetsTest.shape[1]):
        featName = fourVecNames[featI]
        featLabel = fourVecLabels[featI]

         # First in the ML scaling.
        recoJetsTestHist, featBinEdges =  np.histogram(recoJetsScaledTest[:,featI], featBinning)
        truthJetsTestHist, featBinEdges =  np.histogram(truthJetsScaledTest[:,featI], featBinning)
        corrTruthTestHist, featBinEdges =  np.histogram(corrTruthScaledTest[:,featI], featBinning)
        
        recoJetsTrainHist, featBinEdges =  np.histogram(recoJetsScaledTrain[:,featI], featBinning)
        truthJetsTrainHist, featBinEdges =  np.histogram(truthJetsScaledTrain[:,featI], featBinning)
        corrTruthTrainHist, featBinEdges =  np.histogram(corrTruthScaledTrain[:,featI], featBinning)
        
        compareDists([recoJetsTestHist, corrTruthTestHist, truthJetsTestHist], ['reco', 'genreco', 'truth'],
                     featBinEdges, ['k', 'b', 'r'], 'jet', featName+'_genVsReco_test', featLabel)
        compareDists([recoJetsTestHist, corrTruthTestHist, truthJetsTestHist], ['reco', 'genreco', 'truth'],
                     featBinEdges, ['k', 'b', 'r'], 'jet', featName+'_genVsReco_test', featLabel, log=False)

        compareDists([recoJetsTrainHist, corrTruthTrainHist, truthJetsTrainHist], ['reco', 'genreco', 'truth'],
                     featBinEdges, ['k', 'b', 'r'], 'jet', featName+'_genVsReco_test', featLabel)
        compareDists([recoJetsTrainHist, corrTruthTrainHist, truthJetsTrainHist], ['reco', 'genreco', 'truth'],
                     featBinEdges, ['k', 'b', 'r'], 'jet', featName+'_genVsReco_test', featLabel, log=False)

        # Now in the original.
        recoJetsTestHist, featBinEdges =  np.histogram(recoJetsTest[:,featI], origScaleBinning[featI])
        truthJetsTestHist, featBinEdges =  np.histogram(truthJetsTest[:,featI], origScaleBinning[featI])
        corrTruthTestHist, featBinEdges =  np.histogram(corrTruthTest[:,featI], origScaleBinning[featI])

        recoJetsTrainHist, featBinEdges =  np.histogram(recoJetsTrain[:,featI], origScaleBinning[featI])
        truthJetsTrainHist, featBinEdges =  np.histogram(truthJetsTrain[:,featI], origScaleBinning[featI])
        corrTruthTrainHist, featBinEdges =  np.histogram(corrTruthTrain[:,featI], origScaleBinning[featI])

        compareDists([recoJetsTestHist, corrTruthTestHist, truthJetsTestHist], ['reco', 'genreco', 'truth'],
                     featBinEdges, ['k', 'b', 'r'], 'jet', featName+'_genVsReco_origScale_test', featLabel, rounding=varRounding[featI], units=units[featI])
        compareDists([recoJetsTestHist, corrTruthTestHist, truthJetsTestHist], ['reco', 'genreco', 'truth'],
                     featBinEdges, ['k', 'b', 'r'], 'jet', featName+'_genVsReco_origScale_test', featLabel, log=False, rounding=varRounding[featI], units=units[featI])

        compareDists([recoJetsTrainHist, corrTruthTrainHist, truthJetsTrainHist], ['reco', 'genreco', 'truth'],
                     featBinEdges, ['k', 'b', 'r'], 'jet', featName+'_genVsReco_origScale_train', featLabel, rounding=varRounding[featI], units=units[featI])
        compareDists([recoJetsTrainHist, corrTruthTrainHist, truthJetsTrainHist], ['reco', 'genreco', 'truth'],
                     featBinEdges, ['k', 'b', 'r'], 'jet', featName+'_genVsReco_origScale_train', featLabel, log=False, rounding=varRounding[featI], units=units[featI])

    print yTest
    print yGenTest
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Train a NN for regression.')
    parser.add_argument('trainTest', help="Perform training, testing, or both.")
    parser.add_argument('--h5Path', help='Location of training data files.', default='/users/whopkins/temp/rfast008.h5')
    parser.add_argument('--modelName', help='Name of models. Comma delimited if used for tested.', default='model_gan')
    parser.add_argument('--trainingFunc', help='Name of training function to use.', default='trainGAN')
    parser.add_argument('--sourceType', help='What is the source of the data: Delphes or toy detector.', default='Delphes', choices=['Delphes', 'toy', 'other'])
    parser.add_argument('--sourceDSName', help='Name of source data set (the data set you want to transform).', default='truthJets')
    parser.add_argument('--targetDSName', help='Name of target data set (the data set you want another dataset to mimic).', default='recoJets')
    parser.add_argument('--modelFPath', default='models/', help='Path where model should be saved.')
    parser.add_argument('--nTrainSamp', type=int, help='Number of events to use for training', default=200000)
    parser.add_argument('--nTestSamp', type=int, help='Number of events to use for testing', default=100000)
    parser.add_argument('--epochs', type=int, help='Number of epochs', default=20)
    parser.add_argument('--batchSize', type=int, help='Batch size', default=100)
    parser.add_argument('--randomSeed', type=int, help='Seed for randomizor', default=18)
    parser.add_argument('--uniformityProp', type=int, help='Seed for randomizor', default=None)
    parser.add_argument('--hyperParamFPath', type=str, help='Path to pickle file containing the hyper parameters', default='hyperParamsTest.pkl')
    parser.add_argument('--hyperParamKey', type=str, help='Suffix added to file names. Useful for hyperparameter scan.', default='GAN')
    parser.add_argument('--updateHyperparams', action='store_true', help='Update the hyperparameter pickle file.')

    args = parser.parse_args()

    # Let's load the hyperparameter set we need.
    if args.updateHyperparams:
        print "Producing new hyperparameter file."
        produceHyperParams(args.hyperParamFPath)
        allHyperParams = pickle.load(open(args.hyperParamFPath))
    else:
        try:
            allHyperParams = pickle.load(open(args.hyperParamFPath))
        except IOError as e:
            print "Couldn't load "+args.hyperParamFPath
            print "Producing new hyperparameter file."
            produceHyperParams(args.hyperParamFPath)
            allHyperParams = pickle.load(open(args.hyperParamFPath))

    if args.hyperParamKey not in allHyperParams:
        print "Couldn't find hyperparameter set with key", args.hyperParamKey
        sys.exit()
    hyperParams = allHyperParams[args.hyperParamKey]

    # Load training and testing data
    data = prepMatchedData(args.h5Path, args.nTrainSamp, args.nTestSamp, args.randomSeed, sourceType=args.sourceType, uniformityProp=args.uniformityProp);
    np.random.seed(args.randomSeed)

    if 'train' in args.trainTest:
        trainingDict[args.trainingFunc](data, args.modelName, args.modelFPath, hyperParams, args.sourceDSName, args.targetDSName,
                                        args.epochs, args.batchSize, args.hyperParamKey, args.sourceType
        );
    if 'test' in args.trainTest:
        test(data, args.modelName, args.modelFPath, args.sourceDSName, args.targetDSName,
             args.hyperParamKey, args.sourceType
        );
