#!/usr/bin/env python
"""@package docstring
This script trains an NN given an h5 file with object kinematics. 
"""

import sys, os, argparse, h5py, matplotlib
import numpy as np
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from keras.callbacks import EarlyStopping
from keras.models import model_from_json
from keras.layers import Input
from keras.models import Sequential, Model
from keras.optimizers import Adam
from keras.layers.merge import _Merge
from keras import backend as K
from functools import partial
import cPickle as pickle
from sklearn.preprocessing import MinMaxScaler

# Import all models and turn it into a dict. Not super elegant. All models start with the prefix 'model_'. We grab 
import models
modelDict = {}
for comp in dir(models):
    if 'model_' in comp:
        modelDict[comp] = eval('models.'+comp)

print 'Available models'
print '\n'.join(sorted(modelDict.keys()))

from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn import preprocessing

BATCH_SIZE = 100
class RandomWeightedAverage(_Merge):
    """Takes a randomly-weighted average of two tensors. In geometric terms, this outputs a random point on the line
    between each pair of input points.
    Inheriting from _Merge is a little messy but it was the quickest solution I could think of.
    Improvements appreciated."""

    def _merge_function(self, inputs):
        weights = K.random_uniform((BATCH_SIZE, 1, 1, 1))
        return (weights * inputs[0]) + ((1 - weights) * inputs[1])

def prepData(h5Path, nTrainSamp, nTestSamp, randomSeed):
    """! 
    Do the train test splitting after selecting the leading jet info.
    """
    h5f = h5py.File(h5Path)
    truthJets = h5f.get('truthJets').value
    recoJets = h5f.get('recoJets').value

    # Only get the leading jet pt    
    leadJetReco = np.nan_to_num(recoJets.view(np.float32).reshape(recoJets.shape + (-1,))[:,0,:], -999)
    leadJetTruth = np.nan_to_num(truthJets.view(np.float32).reshape(truthJets.shape + (-1,))[:,0,:], -999)
    leadJetRecoPt = leadJetReco[:,0]
    leadJetTruthPt = leadJetTruth[:,0]

    # Let's make some validation plots of the pT, lots of hardcoded ugliness.
    # ptBinning = [i*20 for i in range(31)]
    # plt.clf()
    # fig, (topAX, bottomAX) = plt.subplots(2, 1, gridspec_kw = {'height_ratios':[3, 1]})
    # truthVals, bins, patches = topAX.hist(leadJetTruthPt, bins=ptBinning, histtype='step', log=True)
    # recoVals, bins, patches = topAX.hist(leadJetRecoPt, bins=ptBinning, histtype='step', log=True)
    # topAX.legend(['Truth', 'Reco'], loc=0, borderaxespad=0.)
    # binCenters = (bins[:-1] + bins[1:])/2.
    # bottomAX.plot(binCenters, recoVals/truthVals, 'o')
    # bottomAX.set_xlabel('Leading jet $p_{\mathrm{T}}$ [GeV]')
    # bottomAX.set_ylabel('Reco/Truth')
    # bottomAX.set_ylim((0, 2))

    # topAX.set_ylabel('Events')
    # plt.savefig('leadRecoTruthHist.pdf', bbox_inches='tight')

    # Only select events where the leading jet pT isn't zero (need to understand why this even happens).
    scaler = MinMaxScaler(feature_range=(-1,1))
    xData = leadJetTruth[leadJetTruthPt!=0,:]
    yData = leadJetRecoPt[leadJetTruthPt!=0]/leadJetTruthPt[leadJetTruthPt!=0]
   
    (xTrain, xTest, yTrain, yTest) = train_test_split(xData, yData, test_size=nTestSamp, train_size=nTrainSamp, random_state=randomSeed)
    xTrain = scaler.fit_transform(xTrain)
    xTest = scaler.fit_transform(xTest)
    return (xTrain, xTest, yTrain, yTest)

def train(modelNames, modelFPath, h5Path, sourceDSName, targetDSName, epochs, batchSize, nTrainSamp, nTestSamp, randomSeed, metric='mean_squared_error'):
    """! 
    This function trains a model given an h5 file with a training source and target data set. 
    The source is the data set that you want to make look more like the target, e.g. truth vs reco.
    Most of this is taken from: https://github.com/keras-team/keras-contrib/blob/master/examples/improved_wgan.py.
    Will consider using something like: https://junyanz.github.io/CycleGAN/
    """
    (xTrain, xTest, yTrain, yTest) = prepData(h5Path, nTrainSamp, nTestSamp, randomSeed);
    noiseDim = 100
    jSONSavePathGen = modelFPath+sourceDSName+'_'+targetDSName+'_testGen.json'
    weightsSavePathGen = modelFPath+sourceDSName+'_'+targetDSName+'_testGen.h5'
    generator = modelDict['model_gan_gen'](noiseDim, jSONSavePathGen)

    jSONSavePathDiscr = modelFPath+sourceDSName+'_'+targetDSName+'_testDiscr.json'
    weightsSavePathDiscr = modelFPath+sourceDSName+'_'+targetDSName+'_testDiscr.h5'
    discriminator = modelDict['model_gan_d'](xTrain.shape[1:], jSONSavePathDiscr)

    TRAINING_RATIO = 5  # The training ratio is the number of discriminator updates per generator update. The paper uses 5.
    GRADIENT_PENALTY_WEIGHT = 10  # As per the paper

    # The generator_model is used when we want to train the generator layers.
    # As such, we ensure that the discriminator layers are not trainable.
    # Note that once we compile this model, updating .trainable will have no effect within it. As such, it
    # won't cause problems if we later set discriminator.trainable = True for the discriminator_model, as long
    # as we compile the generator_model first.
    for layer in discriminator.layers:
        layer.trainable = False
    discriminator.trainable = False
    generator_input = Input(shape=(noiseDim,))
    generator_layers = generator(generator_input)
    discriminator_layers_for_generator = discriminator(generator_layers)
    
    generator_model = Model(inputs=[generator_input], outputs=[discriminator_layers_for_generator])
    # We use the Adam paramaters from Gulrajani et al.
    generator_model.compile(optimizer=Adam(0.0001, beta_1=0.5, beta_2=0.9), loss=models.wasserstein_loss)

    # Now that the generator_model is compiled, we can make the discriminator layers trainable.
    for layer in discriminator.layers:
        layer.trainable = True
    for layer in generator.layers:
        layer.trainable = False
    discriminator.trainable = True
    generator.trainable = False

    # The discriminator_model is more complex. It takes both real image samples and random noise seeds as input.
    # The noise seed is run through the generator model to get generated images. Both real and generated images
    # are then run through the discriminator. Although we could concatenate the real and generated images into a
    # single tensor, we don't (see model compilation for why).
    real_samples = Input(shape=xTrain.shape[1:])
    generator_input_for_discriminator = Input(shape=(noiseDim,))
    generated_samples_for_discriminator = generator(generator_input_for_discriminator)
    discriminator_output_from_generator = discriminator(generated_samples_for_discriminator)
    discriminator_output_from_real_samples = discriminator(real_samples)

    # We also need to generate weighted-averages of real and generated samples, to use for the gradient norm penalty.
    averaged_samples = RandomWeightedAverage()([real_samples, generated_samples_for_discriminator])
    # We then run these samples through the discriminator as well. Note that we never really use the discriminator
    # output for these samples - we're only running them to get the gradient norm for the gradient penalty loss.
    averaged_samples_out = discriminator(averaged_samples)

    # The gradient penalty loss function requires the input averaged samples to get gradients. However,
    # Keras loss functions can only have two arguments, y_true and y_pred. We get around this by making a partial()
    # of the function with the averaged samples here.
    partial_gp_loss = partial(models.gradient_penalty_loss,
                              averaged_samples=averaged_samples,
                              gradient_penalty_weight=GRADIENT_PENALTY_WEIGHT)
    partial_gp_loss.__name__ = 'gradient_penalty'  # Functions need names or Keras will throw an error

    # Keras requires that inputs and outputs have the same number of samples. This is why we didn't concatenate the
    # real samples and generated samples before passing them to the discriminator: If we had, it would create an
    # output with 2 * batchSize samples, while the output of the "averaged" samples for gradient penalty
    # would have only batchSize samples.

    # If we don't concatenate the real and generated samples, however, we get three outputs: One of the generated
    # samples, one of the real samples, and one of the averaged samples, all of size batchSize. This works neatly!
    discriminator_model = Model(inputs=[real_samples, generator_input_for_discriminator],
                                outputs=[discriminator_output_from_real_samples,
                                         discriminator_output_from_generator,
                                         averaged_samples_out])
    # We use the Adam paramaters from Gulrajani et al. We use the Wasserstein loss for both the real and generated
    # samples, and the gradient penalty loss for the averaged samples.
    discriminator_model.compile(optimizer=Adam(0.0001, beta_1=0.5, beta_2=0.9),
                                loss=[models.wasserstein_loss,
                                      models.wasserstein_loss,
                                      partial_gp_loss])
    # We make three label vectors for training. positive_y is the label vector for real samples, with value 1.
    # negative_y is the label vector for generated samples, with value -1. The dummy_y vector is passed to the
    # gradient_penalty loss function and is not used.
    positive_y = np.ones((batchSize, 1), dtype=np.float32)
    negative_y = -positive_y
    dummy_y = np.zeros((batchSize, 1), dtype=np.float32)
    discriminator_loss = []
    generator_loss = []
    for epoch in range(epochs):
        np.random.shuffle(xTrain)
        print("Epoch: ", epoch)
        print("Number of batches: ", int(xTrain.shape[0] // batchSize))
        epochDLoss = []
        epochGLoss = []
        minibatches_size = batchSize * TRAINING_RATIO
        for i in range(int(xTrain.shape[0] // (batchSize * TRAINING_RATIO))):
            discriminator_minibatches = xTrain[i * minibatches_size:(i + 1) * minibatches_size]
            for j in range(TRAINING_RATIO):
                image_batch = discriminator_minibatches[j * batchSize:(j + 1) * batchSize]
                noise = np.random.rand(batchSize, noiseDim).astype(np.float32)
                dLoss = discriminator_model.train_on_batch([image_batch, noise], [positive_y, negative_y, dummy_y])
                epochDLoss.append(dLoss)
            epochGLoss.append(generator_model.train_on_batch(np.random.rand(batchSize, noiseDim), positive_y))
        discriminator_loss.append(epochDLoss)
        generator_loss.append(epochGLoss)

    generator.save_weights(weightsSavePathGen)
    discriminator.save_weights(weightsSavePathDiscr)
    generator.summary()
    pickle.dump([discriminator_loss, generator_loss], open('loss.pkl', 'w'))
  
    noise = np.random.rand(nTrainSamp, noiseDim).astype(np.float32)
    xGen = generator.predict(noise)
    # Let's make some validation plots of the pT, lots of hardcoded ugliness.
    ptBinning = [-1+i*0.2 for i in range(21)]
    plt.clf()
    fig, ax = plt.subplots()
    print xTrain.shape, xGen.shape
    ax.hist(xTrain[:,0], bins=ptBinning, histtype='step', log=True)
    ax.hist(xGen[:,0], bins=ptBinning, histtype='step', log=True)
    ax.legend(['Real', 'Gen'], loc=0, borderaxespad=0.)
    ax.set_ylabel('Events')
    plt.savefig('leadTruthHist_duringTrain.pdf', bbox_inches='tight')

def test(modelNames, modelFPath, h5Path, sourceDSName, targetDSName, nTrainSamp, nTestSamp, randomSeed):
    """! 
    This function tests a model given an h5 file with a training source and target data set. 
    """
    (xTrain, xTest, yTrain, yTest) = prepData(h5Path, nTrainSamp, nTestSamp, randomSeed);
    noiseDim = 100
    jSONPathGen = modelFPath+sourceDSName+'_'+targetDSName+'_testGen.json'
    weightsPathGen = modelFPath+sourceDSName+'_'+targetDSName+'_testGen.h5'
    json_file = open(jSONPathGen, 'r')
    model_json = json_file.read()
    json_file.close()
    generator = model_from_json(model_json)
    generator.load_weights(weightsPathGen)
    generator.summary()
    discrLoss, genLoss = pickle.load(open('loss.pkl'))

    genLossAtEndofBatch = [batchLoss[-1] for batchLoss in genLoss]
    discrLoss = np.array(discrLoss)
    discrRealLoss = discrLoss[:,-1,1]
    discrFakeLoss = discrLoss[:,-1,2]
    plt.clf()
    fig, ax = plt.subplots()
    plt.plot(genLossAtEndofBatch)
    ax.set_xlabel('epoch')
    ax.set_ylabel('Wasserstein loss')
    plt.savefig('genLoss.pdf', bbox_inches='tight')
    
    plt.clf()
    fig, ax = plt.subplots()
    plt.plot(discrRealLoss)
    plt.plot(discrFakeLoss)
    ax.set_xlabel('epoch')
    ax.set_ylabel('Wasserstein loss')
    leg = plt.legend(['Real', 'Generated'], loc=0, borderaxespad=0.)
    plt.savefig('discrLoss.pdf', bbox_inches='tight')

    noise = np.random.rand(nTrainSamp, noiseDim).astype(np.float32)
    xGen = generator.predict(noise)
    # Let's make some validation plots of the pT, lots of hardcoded ugliness.
    ptBinning = [-1+i*0.2 for i in range(21)]
    plt.clf()
    fig, ax = plt.subplots()
    print xTrain.shape,xGen.shape
    ax.hist(xTest[:,0], bins=ptBinning, histtype='step', log=True)
    ax.hist(xGen[:,0], bins=ptBinning, histtype='step', log=True)
    ax.legend(['Real', 'Gen'], loc=0, borderaxespad=0.)
    ax.set_ylabel('Events')
    ax.set_xlabel('pT')
    plt.savefig('leadTruthHist.pdf', bbox_inches='tight')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Train a NN for regression.')
    parser.add_argument('trainTest', help="Perform training, testing, or both.")
    parser.add_argument('--h5Path', help='Location of training data files.', default='mg5_ttbar_jet_14TeV.h5')
    parser.add_argument('--modelNames', help='Name of models. Comma delimited if used for tested.', default='model_MLP_MSE')
    parser.add_argument('--sourceDSName', help='Name of source data set (the data set you want to transform).', default='truthJets')
    parser.add_argument('--targetDSName', help='Name of target data set (the data set you want another dataset to mimic).', default='recoJets')
    parser.add_argument('--modelFPath', default='models/', help='Path where model should be saved.')
    parser.add_argument('--nTrainSamp', type=int, help='Number of events to use for training', default=50000)
    parser.add_argument('--nTestSamp', type=int, help='Number of events to use for testing', default=20000)
    parser.add_argument('--epochs', type=int, help='Number of epochs', default=10)
    parser.add_argument('--batchSize', type=int, help='Batch size', default=100)
    parser.add_argument('--randomSeed', type=int, help='Seed for randomizor', default=18)
   
    args = parser.parse_args()
    np.random.seed(args.randomSeed)

    if 'train' in args.trainTest:
        train([modelName for modelName in args.modelNames.split(",")], args.modelFPath,
              args.h5Path, args.sourceDSName, args.targetDSName,
              args.epochs, args.batchSize, args.nTrainSamp, args.nTestSamp, args.randomSeed
        );
    if 'test' in args.trainTest:
        test([modelName for modelName in args.modelNames.split(",")], args.modelFPath,
              args.h5Path, args.sourceDSName, args.targetDSName, args.nTrainSamp, args.nTestSamp, args.randomSeed
        );
    

