#!/usr/bin/env python
"""@package docstring
This script trains an NN given an h5 file with object kinematics. 
"""

import sys, os, argparse, h5py, matplotlib, math
import numpy as np
from copy import deepcopy
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from keras.models import model_from_json
from keras.layers import Input
from keras.models import Sequential, Model
import cPickle as pickle
from utils import *
from functools import partial
# Import all models and turn it into a dict. Not super elegant. All models start with the prefix 'model_'. We grab 
import models
modelDict = {}
for comp in dir(models):
    if 'model_' in comp:
        modelDict[comp] = eval('models.'+comp)

def trainGAN(data, modelName, modelFPath, hyperParams, sourceDSName, targetDSName, epochs, batchSize, hyperParamKey, sourceType):
    """! 
    Vanilla GAN here 
    """
    
    (xTrain, xTest, uniformityWeightsTrain, uniformityWeightsTest, yTrain, yTest, scaler) = data
    inputDim = xTrain.shape[1]
    
    trainingRatio = hyperParams['trainingRatio']
    gen_optimizer = hyperParams['genOptimizer'](lr=hyperParams['genLR'])
    critic_optimizer = hyperParams['discrOptimizer'](hyperParams['discrLR'])

    jSONSavePathDiscr = modelFPath+sourceDSName+'_'+targetDSName+'_'+modelName+'_'+hyperParamKey+'_'+sourceType+'_discr.json'
    weightsSavePathDiscr = modelFPath+sourceDSName+'_'+targetDSName+'_'+modelName+'_'+hyperParamKey+'_'+sourceType+'_discr.h5'
    critic = modelDict[modelName+'_discr'](yTrain.shape[1:],
                                           1,
                                           jSONSavePathDiscr,
                                           outputAct=hyperParams['outputAct_discr'],
                                           hiddenSpaceSize=hyperParams['hiddenSpaceSize_discr'],
                                           activations=hyperParams['activations_discr'])
    
    # We will stop the critic from training when we build the combined generator/critic model.
    setTrainability(critic, trainable=False)

    jSONSavePathGen = modelFPath+sourceDSName+'_'+targetDSName+'_'+modelName+'_'+hyperParamKey+'_'+sourceType+'_gen.json'
    weightsSavePathGen = modelFPath+sourceDSName+'_'+targetDSName+'_'+modelName+'_'+hyperParamKey+'_'+sourceType+'_gen.h5'
    generator = modelDict[modelName+'_gen'](inputDim, yTrain.shape[1],
                                            jSONSavePathGen,
                                            outputAct=hyperParams['outputAct_gen'],
                                            hiddenSpaceSize=hyperParams['hiddenSpaceSize_gen'],
                                            activations=hyperParams['activations_gen'])
    
    # The generator takes truth objects as input and generated reco objects
    generator_input = Input(shape=(inputDim,))
    generator_layers = generator(generator_input)

    # The critic takes generated images as input and determines validity
    critic_out = critic(generator_layers)
    
    combined = Model(generator_input, critic_out)
    combined.compile(loss='binary_crossentropy', optimizer=gen_optimizer, metrics=['accuracy'])
    
    # Now that the combined model has been compiled with the critic not being trainable,
    # we need to turn trainability on again for before we compile the critic.
    setTrainability(critic, trainable=True)
   
    critic.compile(loss='binary_crossentropy', optimizer=critic_optimizer, metrics=['accuracy'])

    critic_loss = []
    critic_loss_real = []
    critic_loss_fake = []
    generator_loss = []

    critic_accuracy = []
    critic_accuracy_real = []
    critic_accuracy_fake = []
    generator_accuracy = []
    
    # Adversarial ground truths
    valid = np.ones((batchSize, 1), dtype=np.float32)* hyperParams['softenFactor']
    fake = np.zeros((batchSize, 1), dtype=np.float32)
    
    for epoch in range(epochs):
        for _ in range(trainingRatio):
            # ---------------------
            #  Train Discriminator
            # ---------------------
            
            # Select a random batch of reco objects
            idx = np.random.randint(0, yTrain.shape[0], batchSize)
            # Use pure noise instead of truth objects
            #truthObjs = np.random.uniform(0, 1, size=[batchSize, xTrain.shape[1]])
            # Use truth as truthObjs
            truthObjs = xTrain[idx]
            recoObjs = yTrain[idx]
            weights = uniformityWeightsTrain[idx]

            # Generate a batch of new images
            genRecoObjs = generator.predict(truthObjs)
            d_loss_real = critic.train_on_batch(recoObjs, valid, sample_weight=weights)
            d_loss_fake = critic.train_on_batch(genRecoObjs, fake, sample_weight=weights)
            d_loss = 0.5 * np.add(d_loss_fake, d_loss_real)

        # ---------------------
        #  Train Generator
        # ---------------------
        idx = np.random.randint(0, yTrain.shape[0], batchSize)
        truthObjs = xTrain[idx]
        weights = uniformityWeightsTrain[idx]
        g_loss = combined.train_on_batch(truthObjs, valid, sample_weight=weights)
        
        critic_loss.append(d_loss[0])
        critic_loss_fake.append(d_loss_fake[0])
        critic_loss_real.append(d_loss_real[0])

        generator_loss.append(g_loss[0])

        critic_accuracy.append(d_loss[1])
        critic_accuracy_fake.append(d_loss_fake[1])
        critic_accuracy_real.append(d_loss_real[1])

        generator_accuracy.append(g_loss[1])
        # Print the progress
        print ("%d [D loss: %f] [G loss: %f]" % (epoch, d_loss[0], g_loss[0]))
        print ("%d [D accuracy: %f] [G accuracy: %f]" % (epoch, d_loss[1], g_loss[1]))

    generator.save_weights(weightsSavePathGen)
    critic.save_weights(weightsSavePathDiscr)
    generator.summary()
    pickle.dump([critic_loss, generator_loss], open(modelName+'_loss.pkl', 'w'))
    
    plt.clf()
    fig, ax = plt.subplots()
    #plt.plot(critic_loss)
    plt.plot(critic_loss_fake)
    #plt.plot(critic_loss_real)
    plt.plot(generator_loss)
    ax.set_xlabel('epoch')
    ax.set_ylabel('Loss')
    leg = plt.legend(['critic gen', 'generator'], loc=0, borderaxespad=0.)
    plt.savefig('loss_'+modelName+'_'+sourceType+'.pdf', bbox_inches='tight')

    plt.clf()
    fig, ax = plt.subplots()
    plt.plot(critic_accuracy)
    plt.plot(critic_accuracy_fake)
    plt.plot(critic_accuracy_real)
    plt.plot(generator_accuracy)
    ax.set_xlabel('epoch')
    ax.set_ylabel('Accuracy')
    leg = plt.legend(['critic', 'fake', 'real', 'generator'], loc=0, borderaxespad=0.)
    plt.savefig('accuracy_'+modelName+'_'+sourceType+'.pdf', bbox_inches='tight')



def trainWGAN(data, modelName, modelFPath, hyperParams, sourceDSName, targetDSName, epochs, batchSize, hyperParamKey, sourceType):
    """! 
    Train a WGAN
    """
    
    (xTrain, xTest, uniformityWeightsTrain, uniformityWeightsTest, yTrain, yTest, scaler) = data
    inputDim = xTrain.shape[1]
    
    trainingRatio = hyperParams['trainingRatio']
    clipValue = hyperParams['clipValue']    
    gen_optimizer = hyperParams['genOptimizer'](lr=hyperParams['genLR'])
    critic_optimizer = hyperParams['discrOptimizer'](hyperParams['discrLR'])

    jSONSavePathDiscr = modelFPath+sourceDSName+'_'+targetDSName+'_'+modelName+'_'+hyperParamKey+'_'+sourceType+'_discr.json'
    weightsSavePathDiscr = modelFPath+sourceDSName+'_'+targetDSName+'_'+modelName+'_'+hyperParamKey+'_'+sourceType+'_discr.h5'
    critic = modelDict[modelName+'_discr'](yTrain.shape[1:],
                                           1,
                                           jSONSavePathDiscr,
                                           outputAct=hyperParams['outputAct_discr'],
                                           hiddenSpaceSize=hyperParams['hiddenSpaceSize_discr'],
                                           activations=hyperParams['activations_discr'])
    
     # We will stop the critic from training when we build the combined generator/critic model.
    setTrainability(critic, trainable=False)

    jSONSavePathGen = modelFPath+sourceDSName+'_'+targetDSName+'_'+modelName+'_'+hyperParamKey+'_'+sourceType+'_gen.json'
    weightsSavePathGen = modelFPath+sourceDSName+'_'+targetDSName+'_'+modelName+'_'+hyperParamKey+'_'+sourceType+'_gen.h5'
    generator = modelDict[modelName+'_gen'](inputDim, yTrain.shape[1],
                                            jSONSavePathGen,
                                            outputAct=hyperParams['outputAct_gen'],
                                            hiddenSpaceSize=hyperParams['hiddenSpaceSize_gen'],
                                            activations=hyperParams['activations_gen'])
    
    # The generator takes truth objects as input and generated reco objects
    generator_input = Input(shape=(inputDim,))
    generator_layers = generator(generator_input)

    # The critic takes generated images as input and determines validity
    critic_out = critic(generator_layers)

    combined = Model(generator_input, critic_out)
    combined.compile(loss=models.wasserstein_loss, optimizer=gen_optimizer, metrics=['accuracy'])

    # Now that the combined model has been compiled with the critic not being trainable,
    # we need to turn trainability on again for before we compile the critic.
    setTrainability(critic, trainable=True)
    critic.compile(loss=models.wasserstein_loss, optimizer=critic_optimizer, metrics=['accuracy'])

    critic_loss = []
    critic_loss_real = []
    critic_loss_fake = []
    generator_loss = []

    critic_accuracy = []
    critic_accuracy_real = []
    critic_accuracy_fake = []
    generator_accuracy = []
    
    # Adversarial ground truths
    valid = np.ones((batchSize, 1), dtype=np.float32)* hyperParams['softenFactor']
    fake = np.zeros((batchSize, 1), dtype=np.float32)
    
    for epoch in range(epochs):
        for _ in range(trainingRatio):
            # ---------------------
            #  Train Discriminator
            # ---------------------
            
            # Select a random batch of reco objects
            idx = np.random.randint(0, yTrain.shape[0], batchSize)
            # Use pure noise instead of truth objects
            #truthObjs = np.random.uniform(0, 1, size=[batchSize, xTrain.shape[1]])
            truthObjs = xTrain[idx]
            recoObjs = yTrain[idx]

            # Generate a batch of new images
            genRecoObjs = generator.predict(truthObjs)
            d_loss_real = critic.train_on_batch(recoObjs, valid)
            d_loss_fake = critic.train_on_batch(genRecoObjs, fake)
            d_loss = 0.5 * np.add(d_loss_fake, d_loss_real)

            # Clip critic weights
            for l in critic.layers:
                weights = l.get_weights()
                weights = [np.clip(w, -clipValue, clipValue) for w in weights]
                l.set_weights(weights)

        # ---------------------
        #  Train Generator
        # ---------------------
        g_loss = combined.train_on_batch(truthObjs, valid)
        
        critic_loss.append(d_loss[0])
        critic_loss_fake.append(d_loss_fake[0])
        critic_loss_real.append(d_loss_real[0])

        generator_loss.append(g_loss[0])

        critic_accuracy.append(d_loss[1])
        critic_accuracy_fake.append(d_loss_fake[1])
        critic_accuracy_real.append(d_loss_real[1])

        generator_accuracy.append(g_loss[1])
        # Plot the progress
        print ("%d [D loss: %f] [G loss: %f]" % (epoch, d_loss[0], g_loss[0]))
        print ("%d [D accuracy: %f] [G accuracy: %f]" % (epoch, d_loss[1], g_loss[1]))

    generator.save_weights(weightsSavePathGen)
    critic.save_weights(weightsSavePathDiscr)
    generator.summary()
    pickle.dump([critic_loss, generator_loss], open(modelName+'_loss.pkl', 'w'))
    
    plt.clf()
    fig, ax = plt.subplots()
    plt.plot(critic_loss)
    plt.plot(critic_loss_fake)
    plt.plot(critic_loss_real)
    plt.plot(generator_loss)
    ax.set_xlabel('epoch')
    ax.set_ylabel('Loss')
    leg = plt.legend(['critic', 'fake', 'real', 'generator'], loc=0, borderaxespad=0.)
    plt.savefig('loss_'+modelName+'_'+sourceType+'.pdf', bbox_inches='tight')

    plt.clf()
    fig, ax = plt.subplots()
    plt.plot(critic_accuracy)
    plt.plot(critic_accuracy_fake)
    plt.plot(critic_accuracy_real)
    plt.plot(generator_accuracy)
    ax.set_xlabel('epoch')
    ax.set_ylabel('Accuracy')
    leg = plt.legend(['critic', 'fake', 'real', 'generator'], loc=0, borderaxespad=0.)
    plt.savefig('accuracy_'+modelName+'_'+sourceType+'.pdf', bbox_inches='tight')
    

def trainWGAN_GP(data, modelName, modelFPath, hyperParams, sourceDSName, targetDSName, epochs, batchSize, hyperParamKey, sourceType):
    """! 
    Train a WGAN-GP. batchSize is currently not tunable. 
    https://github.com/keras-team/keras-contrib/blob/master/examples/improved_wgan.py.
    """
    (xTrain, xTest, uniformityWeightsTrain, uniformityWeightsTest, yTrain, yTest, scaler) = data
    inputDim = xTrain.shape[1]
    jSONSavePathGen = modelFPath+sourceDSName+'_'+targetDSName+'_'+modelName+'_'+hyperParamKey+'_'+sourceType+'_gen.json'
    weightsSavePathGen = modelFPath+sourceDSName+'_'+targetDSName+'_'+modelName+'_'+hyperParamKey+'_'+sourceType+'_gen.h5'
    generator = modelDict[modelName+'_gen'](inputDim, yTrain.shape[1], jSONSavePathGen,
                                            outputAct=hyperParams['outputAct_gen'],
                                            hiddenSpaceSize=hyperParams['hiddenSpaceSize_gen'],
                                            activations=hyperParams['activations_gen'])

    jSONSavePathDiscr = modelFPath+sourceDSName+'_'+targetDSName+'_'+modelName+'_'+hyperParamKey+'_'+sourceType+'_discr.json'
    weightsSavePathDiscr = modelFPath+sourceDSName+'_'+targetDSName+'_'+modelName+'_'+hyperParamKey+'_'+sourceType+'_discr.h5'
    discriminator = modelDict[modelName+'_discr'](yTrain.shape[1:], 1,
                                                  jSONSavePathDiscr,
                                                  outputAct=hyperParams['outputAct_discr'],
                                                  hiddenSpaceSize=hyperParams['hiddenSpaceSize_discr'],
                                                  activations=hyperParams['activations_discr'])
    
    # The training ratio is the number of discriminator updates per generator update. The paper uses 5.
    trainingRatio = hyperParams['trainingRatio']  
    gradientPenaltyWeight = hyperParams['gradientPenaltyWeight']  

    # The generator_model is used when we want to train the generator layers.
    # As such, we ensure that the discriminator layers are not trainable.
    # Note that once we compile this model, updating .trainable will have no effect within it. As such, it
    # won't cause problems if we later set discriminator.trainable = True for the discriminator_model, as long
    # as we compile the generator_model first.
    for layer in discriminator.layers:
        layer.trainable = False
    discriminator.trainable = False
    generator_input = Input(shape=(inputDim,))
    generator_layers = generator(generator_input)
    discriminator_layers_for_generator = discriminator(generator_layers)
    
    generator_model = Model(inputs=[generator_input], outputs=[discriminator_layers_for_generator])
    # We use the Adam paramaters from Gulrajani et al.
    generator_model.compile(optimizer=hyperParams['genOptimizer'](hyperParams['genLR']), loss=models.wasserstein_loss)

    # Now that the generator_model is compiled, we can make the discriminator layers trainable.
    for layer in discriminator.layers:
        layer.trainable = True
    for layer in generator.layers:
        layer.trainable = False
    discriminator.trainable = True
    generator.trainable = False

    # The discriminator_model is more complex. It takes both real image samples and truth jet info as input.
    # The noise seed is run through the generator model to get generated images. Both real and generated images
    # are then run through the discriminator. Although we could concatenate the real and generated images into a
    # single tensor, we don't (see model compilation for why).
    real_samples = Input(shape=yTrain.shape[1:])
    generator_input_for_discriminator = Input(shape=(inputDim,))
    generated_samples_for_discriminator = generator(generator_input_for_discriminator)
    discriminator_output_from_generator = discriminator(generated_samples_for_discriminator)
    discriminator_output_from_real_samples = discriminator(real_samples)

    # We also need to generate weighted-averages of real and generated samples, to use for the gradient norm penalty.
    randomWAvg = models.RandomWeightedAverage()
    randomWAvg.batchSize = batchSize
    averaged_samples = randomWAvg([real_samples, generated_samples_for_discriminator])
    
    # We then run these samples through the discriminator as well. Note that we never really use the discriminator
    # output for these samples - we're only running them to get the gradient norm for the gradient penalty loss.
    averaged_samples_out = discriminator(averaged_samples)

    # The gradient penalty loss function requires the input averaged samples to get gradients. However,
    # Keras loss functions can only have two arguments, y_true and y_pred. We get around this by making a partial()
    # of the function with the averaged samples here.
    partial_gp_loss = partial(models.gradient_penalty_loss,
                              averaged_samples=averaged_samples,
                              gradient_penalty_weight=gradientPenaltyWeight)
    partial_gp_loss.__name__ = 'gradient_penalty'  # Functions need names or Keras will throw an error

    # Keras requires that inputs and outputs have the same number of samples. This is why we didn't concatenate the
    # real samples and generated samples before passing them to the discriminator: If we had, it would create an
    # output with 2 * batchSize samples, while the output of the "averaged" samples for gradient penalty
    # would have only batchSize samples.

    # If we don't concatenate the real and generated samples, however, we get three outputs: One of the generated
    # samples, one of the real samples, and one of the averaged samples, all of size batchSize. This works neatly!
    discriminator_model = Model(inputs=[real_samples, generator_input_for_discriminator],
                                outputs=[discriminator_output_from_real_samples,
                                         discriminator_output_from_generator,
                                         averaged_samples_out])
    
    # We use the Adam paramaters from Gulrajani et al. We use the Wasserstein loss for both the real and generated
    # samples, and the gradient penalty loss for the averaged samples.
    discriminator_model.compile(optimizer=hyperParams['genOptimizer'](hyperParams['genLR']),
                                loss=[models.wasserstein_loss,
                                      models.wasserstein_loss,
                                      partial_gp_loss])
    
    # We make three label vectors for training. positive_y is the label vector for real samples, with value 1.
    # negative_y is the label vector for generated samples, with value -1. The dummy_y vector is passed to the
    # gradient_penalty loss function and is not used.
    positive_y = np.ones((batchSize, 1), dtype=np.float32)
    negative_y = -positive_y
    dummy_y = np.zeros((batchSize, 1), dtype=np.float32)
    discriminator_loss = []
    generator_loss = []
    for epoch in range(epochs):
        np.random.shuffle(yTrain)
        print("Epoch: ", epoch)
        print("Number of batches: ", int(yTrain.shape[0] // batchSize))
        epochDLoss = []
        epochGLoss = []
        minibatches_size = batchSize * trainingRatio
        for i in range(int(yTrain.shape[0] // (batchSize * trainingRatio))):
            discriminator_minibatches = yTrain[i * minibatches_size:(i + 1) * minibatches_size]
            for j in range(trainingRatio):
                image_batch = discriminator_minibatches[j * batchSize:(j + 1) * batchSize]
                noise = xTrain[np.random.randint(batchSize, size=batchSize),:]
                dLoss = discriminator_model.train_on_batch([image_batch, noise], [positive_y, negative_y, dummy_y])
                epochDLoss.append(dLoss)
            epochGLoss.append(generator_model.train_on_batch(xTrain[np.random.randint(batchSize, size=batchSize),:], positive_y))
        discriminator_loss.append(epochDLoss)
        generator_loss.append(epochGLoss)
        print "dLoss", epochDLoss[-1]
        print discriminator_model.metrics_names
        print "gLoss", epochGLoss[-1]
        print generator_model.metrics_names
    generator.save_weights(weightsSavePathGen)
    discriminator.save_weights(weightsSavePathDiscr)
    generator.summary()
    pickle.dump([discriminator_loss, generator_loss], open(modelName+'_loss.pkl', 'w'))
