# Truth2Reco

This package helps convert truth objects (leptons, photons, and jets) to reco objects using machine learning. Several ML methods were explored but the most mature uses a multi-classification neural network. TheNN is taught the resolution of the object features (pT, eta, phi, m) which is defined as the reconstructed quantity over the truth quantity (e.g. pT(reco)/pT(truth). The NN inputs are the features (pT, eta, phi, m, object type) while the output is the binned feature resolution where each bin is treated as a category. An NN is trained for each feature resolution, i.e. there is one NN for pT(reco)/pT(truth), one for eta(reco)/eta(truth), etc. The stochastic nature of a particle detector, i.e. that a particles with identical input features result in many transformed output features, is captured by randomly sampling the NN output pdf for a particular objects. This ensures that for a given identical input, there will be many outputs. 

## Install/setup required environment (with Environment Modules and conda)

Simply source

```installConda.sh```

once and then source

```setupConda.sh```

when you log in. This assumes that you anaconda2 and root (CERN) setup in your environment modules.

## Install/setup with pip

For a local pip installation source:

```installPIP.sh```

and add

```export PATH=$HOME/.local/bin:$PATH```

to your .bash_profile file. 

## Input file format

The inputs should be numpy arrays stored in HDF5 format. These arrays flats and store objects (not events).

MORE INFO NEEDED

## Model inference

Models for each feature are stored in the model directory. The test function in trainRes.py has an example of how to evaluate the NN output (essentially a pdf for the resolution of that object) and how to then randomly sample this pdf. 

## Training and testing.

The trainRes.py can perform both training and testing. To perform both on the example data set included in this repository run this command:

```python trainRes.py traintest```

## Notes on NN structure
I attempted to use a multicategorization NN where there is one large PDF for all the resolution quantities (pT, m, phi, eta). This causes one object to belong to many categories (there is more than one "1" in the 400 bins of the 4x 100 bin resolution categories). This may be ill-defined for the NN since the NN is trying to identify what category an object belongs to. This is known as multi-label classification. I will need to look into this and see whether this will gain us any benefits. One known solution to this is to use many independent NNs but that takes longer to train...

## Recent updates
- All resolutions are now the difference of reco and truth. Phi wrapping for the resolution NN works well now. 

## To do:
- Only allowed sampling of the inefficiency for pT and don't sample once an object is considered out of acceptance. 
- Make trianing and testing modular.
- Add pyTorch implementation of GAN
- Add multiple objects types for NN training. Currently an NN needs to be trained for each object type since the resolutions are different for each object type. Perhaps a Variational Autoencoder or GAN can help with this.
- Benchmark performance of smearing (need to consider HDF5 conversion as well).
- Look at Met and MT after smearing and compare to reconstruction.