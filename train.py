#!/usr/bin/env python
"""@package docstring
This script trains an NN given an h5 file with object kinematics. 
"""

import sys, os, argparse, h5py, matplotlib, math
import numpy as np
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from keras.models import model_from_json

# Import all models and turn it into a dict. Not super elegant. All models start with the prefix 'model_'. We grab 
import models
modelDict = {}
for comp in dir(models):
    if 'model_' in comp:
        modelDict[comp] = eval('models.'+comp)

from utils import *

def train(data, modelName, modelFPath, hyperParams, sourceDSName, targetDSName, epochs, batchSize, hyperParamKey, sourceType):
    """! 
    This function trains a model given an h5 file with a training source and target data set. 
    The source is the data set that you want to make look more like the target, e.g. truth vs reco.
    """
    (xTrain, xTest, uniformityWeightsTrain, uniformityWeightsTest, yTrain, yTest, inputScaler, outputScaler) = data
    jSONSavePath = modelFPath+sourceDSName+'_'+targetDSName+'_'+modelName+'_'+hyperParamKey+'_'+sourceType+'.json'
    weightsSavePath = modelFPath+sourceDSName+'_'+targetDSName+'_'+modelName+'_'+hyperParamKey+'_'+sourceType+'.h5'
    model = modelDict[modelName](xTrain.shape[1:], yTrain.shape[1], jSONSavePath,
                                 outputAct=hyperParams['outputAct'],
                                 hiddenSpaceSize=hyperParams['hiddenSpaceSize'],
                                 activations=hyperParams['activations']
                                 )
    history = model.fit(xTrain, yTrain, epochs=epochs, batch_size=batchSize, shuffle=True, validation_data=(xTest, yTest))
    model.save_weights(weightsSavePath)

    plt.clf()
    fig, ax = plt.subplots()
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    ax.set_xlabel('epoch')
    ax.set_ylabel('Loss')
    leg = plt.legend(['train', 'test'], loc=0, borderaxespad=0.)
    plt.savefig('loss_'+modelName+'_'+sourceType+'.pdf', bbox_inches='tight')


def test(data, modelName, modelFPath, sourceDSName, targetDSName, hyperParamKey, sourceType):
    """! 
    This function tests a model given an h5 file with a training source and target data set. 
    """
    (xTrain, xTest, uniformityWeightsTrain, uniformityWeightsTest, yTrain, yTest, inputScaler, outputScaler) = data
   
    binning = [i*0.01 for i in range(101)]
    profileBinning = [i*0.05 for i in range(25)]
    fourVecBinning = [i*0.05 for i in range(25)]
    
    modelJSONPath = modelFPath+sourceDSName+'_'+targetDSName+'_'+modelName+'_'+hyperParamKey+'_'+sourceType+'.json'
    modelWeightsPath = modelFPath+sourceDSName+'_'+targetDSName+'_'+modelName+'_'+hyperParamKey+'_'+sourceType+'.h5'
    json_file = open(modelJSONPath, 'r')
    model_json = json_file.read()
    json_file.close()
    model = model_from_json(model_json)

    # load weights into new model
    model.load_weights(modelWeightsPath)
    print("Loading", modelJSONPath, modelWeightsPath)
    model.summary()

    yGenTest = model.predict(xTest);
    yGenTrain = model.predict(xTrain);

    #truthObjectsTest = scaler.inverse_transform(xTest[:,:-1])
    #truthObjectsTrain = scaler.inverse_transform(xTrain[:,:-1])
    
    truthObjectsTest = inputScaler.inverse_transform(xTest)
    truthObjectsTrain = inputScaler.inverse_transform(xTrain)
    
    recoObjectsTest = outputScaler.inverse_transform(yTest)
    recoObjectsTrain = outputScaler.inverse_transform(yTrain)

    recoGenObjectsTest = outputScaler.inverse_transform(yGenTest)
    recoGenObjectsTrain = outputScaler.inverse_transform(yGenTrain)

    print(recoGenObjectsTest[:,0])
    # Now let's make some plots.
    for fourVecI in range(4):
        fourVecName       = fourVecNames[fourVecI]
        prettyVarLabel    = ratioLabels[fourVecI]
        rmsLabel          = rmsLabels[fourVecI]
        fourVecLabel      = fourVecLabels[fourVecI]
        fourVecLabelTruth = fourVecLabelsTruth[fourVecI]
        
        recoObjectsTestOrigScaleHist, fourVecBinEdges =  np.histogram(recoObjectsTest[:,fourVecI], origScaleBinning[fourVecI])
        truthObjectsTestOrigScaleHist, fourVecBinEdges =  np.histogram(truthObjectsTest[:,fourVecI], origScaleBinning[fourVecI])
        corrTruthTestOrigScaleHist, fourVecBinEdges =  np.histogram(recoGenObjectsTest[:,fourVecI], origScaleBinning[fourVecI])

        recoObjectsTrainOrigScaleHist, fourVecBinEdges =  np.histogram(recoObjectsTrain[:,fourVecI], origScaleBinning[fourVecI])
        truthObjectsTrainOrigScaleHist, fourVecBinEdges =  np.histogram(truthObjectsTrain[:,fourVecI], origScaleBinning[fourVecI])
        corrTruthTrainOrigScaleHist, fourVecBinEdges =  np.histogram(recoGenObjectsTrain[:,fourVecI], origScaleBinning[fourVecI])

        # compareDists([recoObjectsTestOrigScaleHist, corrTruthTestOrigScaleHist, truthObjectsTestOrigScaleHist], ['reco', 'genreco', 'truth'],
        #              fourVecBinEdges, ['k', 'b', 'r'], 'jet', fourVecName+'_genVsReco_test_origScale', fourVecLabel, ratioEndIndex=1,)
        # compareDists([recoObjectsTestOrigScaleHist, corrTruthTestOrigScaleHist, truthObjectsTestOrigScaleHist], ['reco', 'genreco', 'truth'],
        #              fourVecBinEdges, ['k', 'b', 'r'], 'jet', fourVecName+'_genVsReco_test_origScale', fourVecLabel, log=False, ratioEndIndex=1,)
        
        compareDists([recoObjectsTrainOrigScaleHist, corrTruthTrainOrigScaleHist, truthObjectsTrainOrigScaleHist], ['reco', 'genreco', 'truth'],
                     fourVecBinEdges, ['k', 'b', 'r'], 'jet', fourVecName+'_genVsReco_train_origScale', fourVecLabel, ratioEndIndex=1,)
        compareDists([recoObjectsTrainOrigScaleHist, corrTruthTrainOrigScaleHist, truthObjectsTrainOrigScaleHist], ['reco', 'genreco', 'truth'],
                     fourVecBinEdges, ['k', 'b', 'r'], 'jet', fourVecName+'_genVsReco_train_origScale', fourVecLabel, log=False, ratioEndIndex=1,)

        
        recoObjectsTrainHist, fourVecBinEdges =  np.histogram(yTrain[:,fourVecI], [-0.1+i*0.05 for i in range(23)])
        truthObjectsTrainHist, fourVecBinEdges =  np.histogram(xTrain[:,fourVecI], [-0.1+i*0.05 for i in range(23)])
        corrTruthTrainHist, fourVecBinEdges =  np.histogram(yGenTrain[:,fourVecI], [-0.1+i*0.05 for i in range(23)])

        # compareDists([recoObjectsTestHist, corrTruthTestHist, truthObjectsTestHist], ['reco', 'genreco', 'truth'],
        #              fourVecBinEdges, ['k', 'b', 'r'], 'jet', fourVecName+'_genVsReco_test', fourVecLabel, ratioEndIndex=1,)
        # compareDists([recoObjectsTestHist, corrTruthTestHist, truthObjectsTestHist], ['reco', 'genreco', 'truth'],
        #              fourVecBinEdges, ['k', 'b', 'r'], 'jet', fourVecName+'_genVsReco_test', fourVecLabel, log=False, ratioEndIndex=1,)
        
        compareDists([recoObjectsTrainHist, corrTruthTrainHist, truthObjectsTrainHist], ['reco', 'genreco', 'truth'],
                     fourVecBinEdges, ['k', 'b', 'r'], 'jet', fourVecName+'_genVsReco_train', fourVecLabel, ratioEndIndex=1,)
        compareDists([recoObjectsTrainHist, corrTruthTrainHist, truthObjectsTrainHist], ['reco', 'genreco', 'truth'],
                     fourVecBinEdges, ['k', 'b', 'r'], 'jet', fourVecName+'_genVsReco_train', fourVecLabel, log=False, ratioEndIndex=1,)
        

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Train a NN for regression.')
    parser.add_argument('trainTest', help="Perform training, testing, or both.")
    parser.add_argument('--h5Path', help='Location of training data files.', default='rfast008.h5')
    parser.add_argument('--modelName', help='Name of model.', default='model_ae')
    parser.add_argument('--sourceType', help='What is the source of the data: Delphes or toy detector.', default='Delphes', choices=['Delphes', 'toy', 'other'])
    parser.add_argument('--sourceDSName', help='Name of source data set (the data set you want to transform).', default='truthJets')
    parser.add_argument('--targetDSName', help='Name of target data set (the data set you want another dataset to mimic).', default='recoJets')
    parser.add_argument('--modelFPath', default='models/', help='Path where model should be saved.')
    parser.add_argument('--nTrainSamp', type=int, help='Number of events to use for training', default=200000)
    parser.add_argument('--nTestSamp', type=int, help='Number of events to use for testing', default=100000)
    parser.add_argument('--epochs', type=int, help='Number of epochs', default=10)
    parser.add_argument('--batchSize', type=int, help='Batch size', default=100)
    parser.add_argument('--randomSeed', type=int, help='Seed for randomizor', default=18)
    parser.add_argument('--hyperParamFPath', type=str, help='Path to pickle file containing the hyper parameters', default='hyperParamsTest.pkl')
    parser.add_argument('--hyperParamKey', type=str, help='Suffix added to file names. Useful for hyperparameter scan.', default='autoencoder')
    parser.add_argument('--updateHyperparams', action='store_true', help='Update the hyperparameter pickle file.')
    parser.add_argument('--uniformityProp', type=int, help='Property which will be flattened during training. For example, we may want a flat pT distribution.', default=None)

    args = parser.parse_args()

    # Let's load the hyperparameter set we need.
    if args.updateHyperparams:
        print("Producing new hyperparameter file.")
        produceHyperParams(args.hyperParamFPath)
        allHyperParams = pickle.load(open(args.hyperParamFPath, 'rb'))
    else:
        try:
            allHyperParams = pickle.load(open(args.hyperParamFPath, 'rb'))
        except IOError as e:
            print("Couldn't load "+args.hyperParamFPath)
            print("Producing new hyperparameter file.")
            produceHyperParams(args.hyperParamFPath)
            allHyperParams = pickle.load(open(args.hyperParamFPath, 'rb'))

    if args.hyperParamKey not in allHyperParams:
        print("Couldn't find hyperparameter set with key", args.hyperParamKey)
        sys.exit()
    hyperParams = allHyperParams[args.hyperParamKey]

    # Load training and testing data
    data = prepMatchedData(args.h5Path, args.nTrainSamp, args.nTestSamp, args.randomSeed, sourceType=args.sourceType, uniformityProp=args.uniformityProp);
    np.random.seed(args.randomSeed)

    if 'train' in args.trainTest:
        train(data, args.modelName, args.modelFPath, hyperParams, args.sourceDSName, args.targetDSName,
              args.epochs, args.batchSize, args.hyperParamKey, args.sourceType
        );
    if 'test' in args.trainTest:
        test(data, args.modelName, args.modelFPath, args.sourceDSName, args.targetDSName,
             args.hyperParamKey, args.sourceType
        );
    

