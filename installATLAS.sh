#!/usr/bin/bash

# On node with cvfms
export ATLAS_LOCAL_ROOT_BASE='/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase'
alias setupATLAS='source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh'
setupATLAS -q
lsetup "root 6.10.04-x86_64-slc6-gcc62-opt"

# Install local version of pip
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py --user
export PATH=$HOME/.local/bin:$PATH

# Install required packages. 
pip install keras tensorflow matplotlib scikit-learn --user


