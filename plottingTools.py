#!/usr/bin/env python

import matplotlib
import numpy as np
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import scipy.stats

font = {'family' : 'normal',
        'weight' : 'normal',
        'size'   : 16}

matplotlib.rc('font', **font)

# Some common resolution definitions
ratioLabels = (
    '$p_{\mathrm{T}}^{\mathrm{reco}}-p_{\mathrm{T}}^{\mathrm{truth}}$ [GeV]',
    '$\eta^{\mathrm{reco}}-\eta^{\mathrm{truth}}$',
    '$\phi^{\mathrm{reco}}-\phi^{\mathrm{truth}}$',
    '$\mathrm{m}^{\mathrm{reco}}-\mathrm{m}^{\mathrm{truth}}$ [GeV]',
    'iso$^{\mathrm{reco}}$-iso$^{\mathrm{truth}}$',
    'b$^{\mathrm{reco}}$-b$^{\mathrm{truth}}$',
    )

meanLabels = (
        '<$p_{\mathrm{T}}^{\mathrm{reco}}/p_{\mathrm{T}}^{\mathrm{truth}}$>',
        '<$\eta^{\mathrm{reco}}/\eta^{\mathrm{truth}}$>',
        '<$\phi^{\mathrm{reco}}/\phi^{\mathrm{truth}}$>',
        '<$m^{\mathrm{reco}}/m^{\mathrm{truth}}$>',
    )

resLabels = (
        '$\sigma(p_{\mathrm{T}})-p_{\mathrm{T}}$',
        '$\sigma(\eta)-\eta$',
        '$\sigma(\phi)-\phi$',
        '$\sigma(m)-m$',
    )

resScaledBinning = (
    [i*0.02 for i in range(51)],
    [i*0.02 for i in range(51)],
    [i*0.02 for i in range(51)],
    [i*0.02 for i in range(51)],
)
resNames = (
    'pTRes',
    'etaRes',
    'phiRes',
    'mRes',
    'iso',
    'btag',
    )

featLabels=['$p_{\mathrm{T}}$ [GeV]', '$\eta$', '$\phi$', 'm [GeV]', 'iso', 'bmomfrac']
featLabelsTruth=['$p_{\mathrm{T}}^{\mathrm{truth}}$ [GeV]', '$\eta^{\mathrm{truth}}$', '$\phi^{\mathrm{truth}}$', 'm$^{\mathrm{truth}}$ [GeV]', 'iso$^{\mathrm{truth}$', 'bmomfrac$^{\mathrm{truth}$']
rmsLabels=['$p_{\mathrm{T}}^{\mathrm{reco}}$', '$\eta^{\mathrm{reco}}$', '$\phi^{\mathrm{reco}}$ ', '$\mathrm{m}^{\mathrm{reco}}$', 'iso$^{\mathrm{reco}$', 'bmomfrac$^{\mathrm{reco}$']
featNames = ['pT', 'eta', 'phi', 'm', 'iso', 'bmomfrac']
units = ['GeV', '', '', 'GeV', '', '']


def compareDists(dists, labels, binning, colors, outName, varLabel, prettyVarLabel, log=True, yLabel='Objects', ratioYLabel='Reco/Alt', yLogLowRange=1, yLowRange=0, ratioRange=[0.7, 1.3], rounding=2, units='', plotRatio=True, markerfacecolors=[], markerstyles=[], plotHist=[], plotErr=[], ratioEndIndex=-1, normalize=False):
    """! 
    Compare some x data with generated y data and true y data. The default assumption is that the data will have 
    four dimensions: pT, eta, phi, and m. The binning should have the same dimensions as the data where each element is a list of bins.
    The dists argument should have elements ordered in the following way: baseline distribution, comparison distribution 1, comparison distribution 2, etc.
    """
    binCenters = (binning[:-1] + binning[1:])/2.
    plt.clf()
    if plotRatio:
        fig, (topAX, bottomAX) = plt.subplots(2, 1, gridspec_kw = {'height_ratios':[3, 1]}, sharex=True)
    else:
        fig, topAX = plt.subplots()

    if log:
        topAX.set_yscale('log')

    if markerfacecolors == []:
        markerfacecolors = colors

    if markerstyles == []:
        markerstyles = ['o' for color in colors]

    if plotHist == []:
        plotHist = [False for color in colors]

    if plotErr == []:
        plotErr = [True for color in colors]

    gotLabel = [False for color in colors]

    distRelUnc = [np.sqrt(dist)/dist for dist in dists]
    if normalize:
        dists = [dist/dist.sum() for dist in dists]

    if plotErr[0]:
        topAX.errorbar(binCenters, dists[0], yerr=dists[0]*distRelUnc[0], color=colors[0], label=labels[0], fmt=markerstyles[0], markerfacecolor=markerfacecolors[0])
        gotLabel[0] = True
    if plotHist[0]:
        if gotLabel:
            topAX.step(binCenters, dists[0], color=colors[0], linestyle='dashed', label=labels[0], fillstyle='none', where='mid')
        else:
            topAX.step(binCenters, dists[0], color=colors[0], linestyle='dashed', fillstyle='none', where='mid')

    for distI in range(1, len(dists)):
        if plotHist[distI]:
            topAX.step(binCenters, dists[distI], color=colors[distI], linestyle='dashed', label=labels[distI], fillstyle='none', where='mid')
            gotLabel[distI] = True;
        if plotErr[distI]:
            if gotLabel[distI]:
                topAX.errorbar(binCenters, dists[distI], yerr=dists[distI]*distRelUnc[distI], color=colors[distI], fmt=markerstyles[distI], markerfacecolor=markerfacecolors[distI])
            else:
                topAX.errorbar(binCenters, dists[distI], yerr=dists[distI]*distRelUnc[distI], color=colors[distI], label=labels[distI], fmt=markerstyles[distI], markerfacecolor=markerfacecolors[distI])

    topAX.legend(loc=0, borderaxespad=0.)

    leftLim, rightLim = topAX.get_xlim()
    if plotRatio:
        # This is bad, it should be put in as a parameter but this can be done later.
        ratioRelUnc = np.sqrt(dists[0])/dists[0]
        smallUncIndex =  dists[0]*1.0/dists[1]*ratioRelUnc < 0.1
        tempRatio = dists[0]*1.0/dists[1]
        #print max(tempRatio[smallUncIndex])
        bottomAX.set_xlim(left=leftLim, right=rightLim)
        bottomAX.errorbar(binCenters, dists[0]*1.0/dists[1], yerr=dists[0]*1.0/dists[1]*ratioRelUnc, fmt=markerstyles[1], color=colors[1])
        endIndex = len(dists)
        if ratioEndIndex > 0:
            endIndex = ratioEndIndex
        for compValI in range(2, endIndex):
            bottomAX.errorbar(binCenters, dists[0]*1.0/dists[compValI], yerr=dists[0]*1.0/dists[compValI]*ratioRelUnc, fmt=markerstyles[compValI], fillstyle='none', color=colors[compValI])
        bottomAX.set_xlabel(' jet '+prettyVarLabel)
        bottomAX.set_ylabel(ratioYLabel)
        bottomAX.set_ylim(ratioRange)
        bottomAX.grid(True)
    else:
        topAX.set_xlabel(' jet '+prettyVarLabel)
        topAX.set_ylabel(ratioYLabel)
    binSize = binning[1]-binning[0]
    if rounding > 0:
        binSizeStr = '{0:.{1}f} {2:}'.format(round(binSize,rounding), rounding, units)
    else:
        binSizeStr = '{0:d} {1:}'.format(int(binSize), units)
       
    topAX.set_ylabel(yLabel+" / "+binSizeStr)
    if log and yLogLowRange != None:
        yRange = (yLogLowRange, topAX.get_ylim()[1]*30)
    else:
        yRange = (yLowRange, topAX.get_ylim()[1]*1.3)
    topAX.set_ylim(yRange)

    fig.subplots_adjust(hspace=0)
    fig.canvas.draw()

    yLabels = [item.get_text() for item in topAX.get_yticklabels()]
    yLabels[0] = ''
    if log:
        yLabels[1] = ''
    topAX.set_yticklabels(yLabels)

    if log:
        plt.savefig(outName+'_'+varLabel+'_log.pdf', bbox_inches='tight')
    else:
        plt.savefig(outName+'_'+varLabel+'.pdf', bbox_inches='tight')

    plt.close('all');

def compareMeans(x, ys, labels, binning, colors, outName, varLabel, prettyVarLabel, log=True, yLabel='AU', yRange=None):
    """! 
    Compare the means of a list of variables given in ys as function of x. For example, the mean of reco/truth pT as a function of pT.
    """
    binCenters = (binning[:-1] + binning[1:])/2.

    plt.clf()
    fig, ax = plt.subplots()
    for yI in range(len(ys)):
        fillstyle='full'
        if yI > 0:
            fillstyle = 'none'
        mean = scipy.stats.binned_statistic(x, ys[yI], statistic='mean', bins=binning, range=None)[0]
        std = scipy.stats.binned_statistic(x, ys[yI], statistic=np.std, bins=binning, range=None)[0]
        #ax.errorbar(binCenters, mean, yerr=std, fmt='o', color=colors[yI], fillstyle=fillstyle)
        ax.plot(binCenters, mean, 'o', color=colors[yI], fillstyle=fillstyle)
        
    ax.legend(labels, loc="best", borderaxespad=0.)
    ax.set_xlabel('Jet '+prettyVarLabel)
    ax.set_ylabel(yLabel)
    if yRange:
        ax.set_ylim(yRange)
    fig.subplots_adjust(hspace=0)

    if log:
        plt.savefig(outName+'_'+varLabel+'_mean_log.pdf', bbox_inches='tight')
    else:
        plt.savefig(outName+'_'+varLabel+'_mean.pdf', bbox_inches='tight')
    plt.close('all')
    
def compareRMSs(ys, labels, binning, colors, outName, varLabel, prettyVarLabel, log=True, yLabel='AU', yRange=None):
    """! 
    Compare the RMS of a list of variables given in ys as function of x. For example, the mean of reco/truth pT as a function of pT.
    """
    binCenters = (binning[:-1] + binning[1:])/2.

    plt.clf()
    fig, ax = plt.subplots()
    for yI in range(len(ys)):
        fillstyle='full'
        if yI > 0:
            fillstyle = 'none'
        std = scipy.stats.binned_statistic(ys[yI], ys[yI], statistic=np.std, bins=binning, range=None)[0]
        nEntries = scipy.stats.binned_statistic(ys[yI], ys[yI], statistic='count', bins=binning, range=None)[0]
        stdErr = np.sqrt(np.square(std)/(2.0*nEntries))
        ax.errorbar(binCenters, std*1./binCenters, yerr=stdErr*1./binCenters, fmt='o', color=colors[yI], fillstyle=fillstyle)
        
    ax.legend(labels, loc="best", borderaxespad=0.)
    ax.set_xlabel('Jet '+prettyVarLabel)
    ax.set_ylabel(yLabel)
    if yRange:
        ax.set_ylim(yRange)

    if log:
        plt.savefig(outName+'_'+varLabel+'_std_log.pdf', bbox_inches='tight')
    else:
        plt.savefig(outName+'_'+varLabel+'_std.pdf', bbox_inches='tight')
    plt.close('all')
